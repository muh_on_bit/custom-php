<?php

class Base extends Database{
	protected $MANDRILL;
	protected $MAILGUN;
	protected $AUTHORIZE;
	public $DB;
	public $salt1 = PASSWORD_SALT_1;
	public $salt2 = PASSWORD_SALT_2;
	public $config_params;


	public function __construct($config_params=array()){
	   $this->config_params = $config_params;
		$this->DB = new Database();
	   $this->MANDRILL = new Mandrill;
	   $this->MAILGUN = new Mailgun;
	   if(ENABLE_AUTHORIZE) $this->AUTHORIZE = new AuthorizeNetAIM;
    }

	public function getTableFields($table_name) {
		$query = "SHOW COLUMNS FROM `".$table_name."`";
		$data = $this->DB->fetchAll($query);

		$fields_array = array();
		foreach($data as $row) {
			array_push($fields_array, $row['Field']);
		}
		return $fields_array;
	}

	public function redirect($location) {
		$location = "Location:".$location;
		header($location);
		exit;
	}

	public function currentURL($_GET2) {
		$pageURL = 'http';
		if (isset($_SERVER["HTTPS"])){
			if ($_SERVER["HTTPS"] == "on") {
				$pageURL .= "s";
			}
		}
		$pageURL .= "://";
		if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		} else {
			$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		}
		if(count($_GET2)>0) {
			$pageURL .= '?';
		}
		foreach($_GET2 as $name => $value) {
			$pageURL .= $name . '=' . $value;
		}
		return $pageURL;
	}

	public function currentPage() {
		return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
	}

	public function getCurrentDirectory() {
		$pathinfo = pathinfo( $this->currentURL($_GET) );
		$dirname_array = explode("/", $pathinfo['dirname']);
		$num_directories = count($dirname_array);
		return $dirname_array[$num_directories - 1];
	}

	 public function getAllStates($all_states=false, $fifty_states=false) {
		$state_list = array(
			'AL'=>"Alabama",
			'AK'=>"Alaska",
			'AZ'=>"Arizona",
			'AR'=>"Arkansas",
			'CA'=>"California",
			'CO'=>"Colorado",
			'CT'=>"Connecticut",
			'DE'=>"Delaware",
			'DC'=>"District Of Columbia",
			'FL'=>"Florida",
			'GA'=>"Georgia",
			'HI'=>"Hawaii",
			'ID'=>"Idaho",
			'IL'=>"Illinois",
			'IN'=>"Indiana",
			'IA'=>"Iowa",
			'KS'=>"Kansas",
			'KY'=>"Kentucky",
			'LA'=>"Louisiana",
			'ME'=>"Maine",
			'MD'=>"Maryland",
			'MA'=>"Massachusetts",
			'MI'=>"Michigan",
			'MN'=>"Minnesota",
			'MS'=>"Mississippi",
			'MO'=>"Missouri",
			'MT'=>"Montana",
			'NE'=>"Nebraska",
			'NV'=>"Nevada",
			'NH'=>"New Hampshire",
			'NJ'=>"New Jersey",
			'NM'=>"New Mexico",
			'NY'=>"New York",
			'NC'=>"North Carolina",
			'ND'=>"North Dakota",
			'OH'=>"Ohio",
			'OK'=>"Oklahoma",
			'OR'=>"Oregon",
			'PA'=>"Pennsylvania",
			'RI'=>"Rhode Island",
			'SC'=>"South Carolina",
			'SD'=>"South Dakota",
			'TN'=>"Tennessee",
			'TX'=>"Texas",
			'UT'=>"Utah",
			'VT'=>"Vermont",
			'VA'=>"Virginia",
			'WA'=>"Washington",
			'WV'=>"West Virginia",
			'WI'=>"Wisconsin",
			'WY'=>"Wyoming"
		);

		if($all_states){
			$state_list = array_merge(array('ALL'=>"All States"), $state_list);
		}

		if($fifty_states){
			unset($state_list['DC']);
		}

		return $state_list;
	}

	public function getAllProvinces() {
		$canadian_states = array(
			"BC"=>"British Columbia",
			"ON"=>"Ontario",
			"NL"=>"Newfoundland and Labrador",
			"NS"=>"Nova Scotia",
			"PE"=>"Prince Edward Island",
			"NB"=>"New Brunswick",
			"QC"=>"Quebec",
			"MB"=>"Manitoba",
			"SK"=>"Saskatchewan",
			"AB"=>"Alberta",
			"NT"=>"Northwest Territories",
			"NU"=>"Nunavut",
			"YT"=>"Yukon Territory");
		return $canadian_states;
	}

	public function getAllCountries() {
		$countries = array(
			'AF'=>'AFGHANISTAN',
			'AL'=>'ALBANIA',
			'DZ'=>'ALGERIA',
			'AS'=>'AMERICAN SAMOA',
			'AD'=>'ANDORRA',
			'AO'=>'ANGOLA',
			'AI'=>'ANGUILLA',
			'AQ'=>'ANTARCTICA',
			'AG'=>'ANTIGUA AND BARBUDA',
			'AR'=>'ARGENTINA',
			'AM'=>'ARMENIA',
			'AW'=>'ARUBA',
			'AU'=>'AUSTRALIA',
			'AT'=>'AUSTRIA',
			'AZ'=>'AZERBAIJAN',
			'BS'=>'BAHAMAS',
			'BH'=>'BAHRAIN',
			'BD'=>'BANGLADESH',
			'BB'=>'BARBADOS',
			'BY'=>'BELARUS',
			'BE'=>'BELGIUM',
			'BZ'=>'BELIZE',
			'BJ'=>'BENIN',
			'BM'=>'BERMUDA',
			'BT'=>'BHUTAN',
			'BO'=>'BOLIVIA',
			'BA'=>'BOSNIA AND HERZEGOVINA',
			'BW'=>'BOTSWANA',
			'BV'=>'BOUVET ISLAND',
			'BR'=>'BRAZIL',
			'IO'=>'BRITISH INDIAN OCEAN TERRITORY',
			'BN'=>'BRUNEI DARUSSALAM',
			'BG'=>'BULGARIA',
			'BF'=>'BURKINA FASO',
			'BI'=>'BURUNDI',
			'KH'=>'CAMBODIA',
			'CM'=>'CAMEROON',
			'CA'=>'CANADA',
			'CV'=>'CAPE VERDE',
			'KY'=>'CAYMAN ISLANDS',
			'CF'=>'CENTRAL AFRICAN REPUBLIC',
			'TD'=>'CHAD',
			'CL'=>'CHILE',
			'CN'=>'CHINA',
			'CX'=>'CHRISTMAS ISLAND',
			'CC'=>'COCOS (KEELING) ISLANDS',
			'CO'=>'COLOMBIA',
			'KM'=>'COMOROS',
			'CG'=>'CONGO',
			'CD'=>'CONGO, THE DEMOCRATIC REPUBLIC OF THE',
			'CK'=>'COOK ISLANDS',
			'CR'=>'COSTA RICA',
			'CI'=>'COTE D IVOIRE',
			'HR'=>'CROATIA',
			'CU'=>'CUBA',
			'CY'=>'CYPRUS',
			'CZ'=>'CZECH REPUBLIC',
			'DK'=>'DENMARK',
			'DJ'=>'DJIBOUTI',
			'DM'=>'DOMINICA',
			'DO'=>'DOMINICAN REPUBLIC',
			'TP'=>'EAST TIMOR',
			'EC'=>'ECUADOR',
			'EG'=>'EGYPT',
			'SV'=>'EL SALVADOR',
			'GQ'=>'EQUATORIAL GUINEA',
			'ER'=>'ERITREA',
			'EE'=>'ESTONIA',
			'ET'=>'ETHIOPIA',
			'FK'=>'FALKLAND ISLANDS (MALVINAS)',
			'FO'=>'FAROE ISLANDS',
			'FJ'=>'FIJI',
			'FI'=>'FINLAND',
			'FR'=>'FRANCE',
			'GF'=>'FRENCH GUIANA',
			'PF'=>'FRENCH POLYNESIA',
			'TF'=>'FRENCH SOUTHERN TERRITORIES',
			'GA'=>'GABON',
			'GM'=>'GAMBIA',
			'GE'=>'GEORGIA',
			'DE'=>'GERMANY',
			'GH'=>'GHANA',
			'GI'=>'GIBRALTAR',
			'GR'=>'GREECE',
			'GL'=>'GREENLAND',
			'GD'=>'GRENADA',
			'GP'=>'GUADELOUPE',
			'GU'=>'GUAM',
			'GT'=>'GUATEMALA',
			'GN'=>'GUINEA',
			'GW'=>'GUINEA-BISSAU',
			'GY'=>'GUYANA',
			'HT'=>'HAITI',
			'HM'=>'HEARD ISLAND AND MCDONALD ISLANDS',
			'VA'=>'HOLY SEE (VATICAN CITY STATE)',
			'HN'=>'HONDURAS',
			'HK'=>'HONG KONG',
			'HU'=>'HUNGARY',
			'IS'=>'ICELAND',
			'IN'=>'INDIA',
			'ID'=>'INDONESIA',
			'IR'=>'IRAN, ISLAMIC REPUBLIC OF',
			'IQ'=>'IRAQ',
			'IE'=>'IRELAND',
			'IL'=>'ISRAEL',
			'IT'=>'ITALY',
			'JM'=>'JAMAICA',
			'JP'=>'JAPAN',
			'JO'=>'JORDAN',
			'KZ'=>'KAZAKSTAN',
			'KE'=>'KENYA',
			'KI'=>'KIRIBATI',
			'KP'=>'KOREA DEMOCRATIC PEOPLES REPUBLIC OF',
			'KR'=>'KOREA REPUBLIC OF',
			'KW'=>'KUWAIT',
			'KG'=>'KYRGYZSTAN',
			'LA'=>'LAO PEOPLES DEMOCRATIC REPUBLIC',
			'LV'=>'LATVIA',
			'LB'=>'LEBANON',
			'LS'=>'LESOTHO',
			'LR'=>'LIBERIA',
			'LY'=>'LIBYAN ARAB JAMAHIRIYA',
			'LI'=>'LIECHTENSTEIN',
			'LT'=>'LITHUANIA',
			'LU'=>'LUXEMBOURG',
			'MO'=>'MACAU',
			'MK'=>'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF',
			'MG'=>'MADAGASCAR',
			'MW'=>'MALAWI',
			'MY'=>'MALAYSIA',
			'MV'=>'MALDIVES',
			'ML'=>'MALI',
			'MT'=>'MALTA',
			'MH'=>'MARSHALL ISLANDS',
			'MQ'=>'MARTINIQUE',
			'MR'=>'MAURITANIA',
			'MU'=>'MAURITIUS',
			'YT'=>'MAYOTTE',
			'MX'=>'MEXICO',
			'FM'=>'MICRONESIA, FEDERATED STATES OF',
			'MD'=>'MOLDOVA, REPUBLIC OF',
			'MC'=>'MONACO',
			'MN'=>'MONGOLIA',
			'MS'=>'MONTSERRAT',
			'MA'=>'MOROCCO',
			'MZ'=>'MOZAMBIQUE',
			'MM'=>'MYANMAR',
			'NA'=>'NAMIBIA',
			'NR'=>'NAURU',
			'NP'=>'NEPAL',
			'NL'=>'NETHERLANDS',
			'AN'=>'NETHERLANDS ANTILLES',
			'NC'=>'NEW CALEDONIA',
			'NZ'=>'NEW ZEALAND',
			'NI'=>'NICARAGUA',
			'NE'=>'NIGER',
			'NG'=>'NIGERIA',
			'NU'=>'NIUE',
			'NF'=>'NORFOLK ISLAND',
			'MP'=>'NORTHERN MARIANA ISLANDS',
			'NO'=>'NORWAY',
			'OM'=>'OMAN',
			'PK'=>'PAKISTAN',
			'PW'=>'PALAU',
			'PS'=>'PALESTINIAN TERRITORY, OCCUPIED',
			'PA'=>'PANAMA',
			'PG'=>'PAPUA NEW GUINEA',
			'PY'=>'PARAGUAY',
			'PE'=>'PERU',
			'PH'=>'PHILIPPINES',
			'PN'=>'PITCAIRN',
			'PL'=>'POLAND',
			'PT'=>'PORTUGAL',
			'PR'=>'PUERTO RICO',
			'QA'=>'QATAR',
			'RE'=>'REUNION',
			'RO'=>'ROMANIA',
			'RU'=>'RUSSIAN FEDERATION',
			'RW'=>'RWANDA',
			'SH'=>'SAINT HELENA',
			'KN'=>'SAINT KITTS AND NEVIS',
			'LC'=>'SAINT LUCIA',
			'PM'=>'SAINT PIERRE AND MIQUELON',
			'VC'=>'SAINT VINCENT AND THE GRENADINES',
			'WS'=>'SAMOA',
			'SM'=>'SAN MARINO',
			'ST'=>'SAO TOME AND PRINCIPE',
			'SA'=>'SAUDI ARABIA',
			'SN'=>'SENEGAL',
			'SC'=>'SEYCHELLES',
			'SL'=>'SIERRA LEONE',
			'SG'=>'SINGAPORE',
			'SK'=>'SLOVAKIA',
			'SI'=>'SLOVENIA',
			'SB'=>'SOLOMON ISLANDS',
			'SO'=>'SOMALIA',
			'ZA'=>'SOUTH AFRICA',
			'GS'=>'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS',
			'ES'=>'SPAIN',
			'LK'=>'SRI LANKA',
			'SD'=>'SUDAN',
			'SR'=>'SURINAME',
			'SJ'=>'SVALBARD AND JAN MAYEN',
			'SZ'=>'SWAZILAND',
			'SE'=>'SWEDEN',
			'CH'=>'SWITZERLAND',
			'SY'=>'SYRIAN ARAB REPUBLIC',
			'TW'=>'TAIWAN, PROVINCE OF CHINA',
			'TJ'=>'TAJIKISTAN',
			'TZ'=>'TANZANIA, UNITED REPUBLIC OF',
			'TH'=>'THAILAND',
			'TG'=>'TOGO',
			'TK'=>'TOKELAU',
			'TO'=>'TONGA',
			'TT'=>'TRINIDAD AND TOBAGO',
			'TN'=>'TUNISIA',
			'TR'=>'TURKEY',
			'TM'=>'TURKMENISTAN',
			'TC'=>'TURKS AND CAICOS ISLANDS',
			'TV'=>'TUVALU',
			'UG'=>'UGANDA',
			'UA'=>'UKRAINE',
			'AE'=>'UNITED ARAB EMIRATES',
			'GB'=>'UNITED KINGDOM',
			'USA'=>'USA',
			'UM'=>'UNITED STATES MINOR OUTLYING ISLANDS',
			'UY'=>'URUGUAY',
			'UZ'=>'UZBEKISTAN',
			'VU'=>'VANUATU',
			'VE'=>'VENEZUELA',
			'VN'=>'VIET NAM',
			'VG'=>'VIRGIN ISLANDS, BRITISH',
			'VI'=>'VIRGIN ISLANDS, U.S.',
			'WF'=>'WALLIS AND FUTUNA',
			'EH'=>'WESTERN SAHARA',
			'YE'=>'YEMEN',
			'YU'=>'YUGOSLAVIA',
			'ZM'=>'ZAMBIA',
			'ZW'=>'ZIMBABWE'
		);
		return $countries;
	}

	public function getAllContinents() {
		$continents = array(
			'North America',
			'South America',
			'Europe',
			'Africa',
			'Asia'
		);
		return $continents;
	}

	public function getCountryContinents() {
		$country_continents = array(
			'Canada' => 'North America',
			'USA' => 'North America',
			'Puerto Rico' => 'North America',
			'Finland' => 'Europe',
			'Greece' => 'Europe',
			'South Africa' => 'Africa',
			'China' => 'Asia',
			'Indonesia' => 'Asia',
			'Japan' => 'Asia',
			'Viet Nam' => 'Asia'
		);
		return $country_continents;
	}

	public function getAllMonths() {
		$months = array(
			'01' => 'January',
			'02' => 'February',
			'03' => 'March',
			'04' => 'April',
			'05' => 'May',
			'06' => 'June',
			'07' => 'July',
			'08' => 'August',
			'09' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December'
		);
		return $months;
	}

	public function getConfigs() {
		$query = "SELECT * FROM `configs`";
		$data = $this->DB->fetchAll($query);

		foreach($data as $row) {
			$configs[$row['name']] = $row['value'];
		}
		if(!isset($configs)){
			$configs="";
		}
		return $configs;
	}

	public function addConfig($_POST2) {
		if(!$_POST2['config_name'] || !$_POST2['config_value']){
			return array("result" => false, "message"=>"Config needs a config name and config value");
		}
		$query = "INSERT INTO `configs` SET `name` = '".$_POST2['config_name']."', `value` = '".$_POST2['config_value']."'";
		$result=$this->DB->query($query);

		if($result['status']) {
			return array("status" => true, "message"=>"Config added successfully");
		} else {
			return array("status" => false, "message"=>"Config could not be added");
		}
	}

	public function deleteConfigByName($name) {
		$query = "DELETE FROM `configs` WHERE `name` = '".$name."'";
		$result=$this->DB->query($query);

		if($result['status']) {
			return array("status" => true, "message"=>"Config deleted successfully");
		} else {
			return array("status" => false, "message"=>"Config could not be deleted");
		}
	}

	public function validateEmail($email) {
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
	   		return true;
		}
	}

	public function cleanString($dirty_string) {
		$dirty_string=str_replace(array('\r', '\n'),  '' ,$dirty_string);
		$clean_string=stripslashes($dirty_string);
		return $clean_string;
	}

	public function parseHTML($string) {
		$html_entities = array(
 			'&reg;',
 			'&copy;',
 			'&trade;',
		);
		$html_tags = array(
			'<sup>&reg;</sup>',
 			'<sup>&copy;</sup>',
 			'<sup>&trade;</sup>',
		);
		$result = str_replace($html_entities, $html_tags, $string);
		return $result;
	}

	/***Create, Insert, Update Configs***/
	public function getConfigsByTableName($tablename) {
		$query = ("SELECT * FROM `".$tablename."`");
		$data = $this->DB->fetchAll($query);

		foreach($data as $row) {
			$configs[$row['name']] = $row['value'];
		}
		return $configs;
	}

	public function insertUpdateConfigs($array, $tablename) {
		foreach($array as $name => $value) {
			$config_exists = $this->configExists($name, $tablename);
			if($config_exists) {
				$this->updateConfig($name, $value, $tablename);
			} else {
				$this->insertConfig($name, $value, $tablename);
			}
		}
	}

	public function insertConfig($name, $value, $tablename) {
		$query = "INSERT INTO `".$tablename."` SET `name` = '".$name."', `value` = '".$value."'";
		$result=$this->DB->query($query);

		if($result['status']) {
			return true;
		} else {
			return false;
		}
	}

	public function updateConfig($name, $value, $tablename) {
		$query = "UPDATE `".$tablename."` SET `value` = '".$value."' WHERE `name` = '".$name."'";
		$result=$this->DB->query($query);

		if($result['status']) {
			return true;
		} else {
			return false;
		}
	}

	public function configExists($name, $tablename) {
		$query = "SELECT * FROM `".$tablename."` WHERE `name` = '".$name."'";
		$data = $this->DB->fetchAll($query);

		if(count($data) > 0) {

			return true;
		} else {
			return false;
		}
	}

	public function getDelimitedArray($subject, $delimiter) {
		$names= explode($delimiter, $subject);
		return $names;
	}

	public function create_password($mask) {
		//web/admin password
		//echo create_password('CcX#CcX#');
	  	//$extended_chars = "!@#$%^&*()";
		$extended_chars = "!$^&()";
		$length = strlen($mask);
		$pwd = '';
		for ($c=0;$c<$length;$c++) {
			$ch = $mask[$c];
			switch ($ch) {
				case '#':
					$p_char = rand(0,9);
					break;
				case 'C':
					$p_char = chr(rand(65,90));
					break;
				case 'c':
					$p_char = chr(rand(97,122));
					break;
				case 'X':
					do {
					  $p_char = rand(65,122);
					} while ($p_char > 90 && $p_char < 97);
					$p_char = chr($p_char);
					break;
				case '!':
					$p_char = $extended_chars[rand(0,strlen($extended_chars)-1)];
					break;
			}
			$pwd .= $p_char;
		}
	  	return trim($pwd);
	}

	public function getDirectoryList ($directory){
		$results = array();

		$handler = opendir($directory);

		while ($file = readdir($handler)) {
		  if ($file != "." && $file != "..") {
			$results[] = $file;
		  }
		}
		closedir($handler);

		return $results;
  	}

	public function formatPhone($num)
	{
		$num = ereg_replace('[^0-9]', '', $num);

		$len = strlen($num);
		if($len == 7)
			$num = preg_replace('/([0-9]{3})([0-9]{4})/', '$1-$2', $num);
		elseif($len == 10)
			$num = preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '($1) $2-$3', $num);

		return $num;
	}

	public function firstOfMonth() {
		return date("Y-m-d h:i:s", strtotime(date('m').'/01/'.date('Y').' 00:00:00'));
	}

	public function lastOfMonth() {
		return date("Y-m-d h:i:s", strtotime('-1 second',strtotime('+1 month',strtotime(date('m').'/01/'.date('Y').' 00:00:00'))));
	}

	public function add_date($givendate,$day,$mth,$yr) {
		$cd = strtotime($givendate);
		$newdate = date('Y-m-d h:i:s', mktime(date('h',$cd),
		date('i',$cd), date('s',$cd), date('m',$cd)+$mth,
		date('d',$cd)+$day, date('Y',$cd)+$yr));
		return $newdate;
	}

	public function submitForm($form_settings, $_POST2, $_FILES2){
		if($form_settings['captcha']){
			//set POST variables
			$url = 'https://www.google.com/recaptcha/api/siteverify';
			$fields = array(
								'secret' => CAPTCHA_PRIVATE_KEY,
								'response' => $_POST2['g-recaptcha-response'],
								'remoteip' => $_SERVER['REMOTE_ADDR'],
							);

			//open connection
			$ch = curl_init();

			curl_setopt($ch,CURLOPT_URL, $url);
			curl_setopt($ch,CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			//execute post
			$resp = json_decode(curl_exec($ch), true);

			//close connection
			curl_close($ch);

			if (!$resp['success']) {
				// What happens when the CAPTCHA was entered incorrectly
				$error = array('status' => false, 'message' => 'The reCAPTCHA wasn\'t entered correctly. Go back and try it again.');
				return $error;
			}
		}

		//Check required fields
		if(is_array($form_settings['required_fields'])){
			foreach($form_settings['required_fields'] as $key => $field) {
				if($_POST2[$field]=="") {
					$error = array(
						'status' => false,
						'message' => 'Please fill in all the required fields.'
					);
					return $error;
				}else if ($_POST2[$field]=="email"){
					//verify email is correct
					if (!filter_var($_POST2['email'], FILTER_VALIDATE_EMAIL)) {
						$error = array(
							'result' => "error",
							'message' => 'Invalid email format.'
						);
						return $error;
					}
				}
			}
		}

		//honeypot
		if($form_settings['honeypot']['enabled']){
			if($_POST2[$form_settings['honeypot']['field_name']]){
				return false;
			}
		}

		//Submit file
		if($form_settings['files']['enabled']){
			foreach($form_settings['files']['filenames'] as $file){
				if($_FILES2[$file]['name']!=""){
					$result=$this->uploadFile($_FILES2[$file]);
					if($result['status']){
						$_POST2[$file] = $result['filename'];
						if($form_settings['database']['enabled']){
							$form_settings['database']['query'].=', `'   .$file.'`="' . $result['filename'] . '"';
						}
					}else{
						return array("status" => false, "message"=> $result['message']);
					}

				}
			}
		}

		//database
		if($form_settings['database']['enabled']){
			$query = $form_settings['database']['query'];
			$result=$this->DB->query($query);

			$insert_id=$result['id'];
			if($form_settings['database']['enable_id']){
				$_POST2['id']=$insert_id;
			}

			foreach($form_settings['email_confirmation']['replace_fields']['array_mapping'] as $key=>$value){
				if($form_settings['database']['enable_id'] && $key=="ID"){
					$form_settings['email_confirmation']['replace_fields']['array_mapping']['ID']=$_POST2['id'];
				}
			}
			if(!$result['status']){
				$error = array(
					'status' => false,
					'message' => 'Could not save to database.'
				);
				return $error;
			}
		}

		//replace mappings
		if($form_settings['email']['replace_fields']['enabled'] && !$form_settings['email']['replace_fields']['use_array']){
			$form_settings['email']['body']=$this->replaceMappings($form_settings['email']['body'], $form_settings['email']['replace_fields']['array_mapping']);
		}else if($form_settings['email']['replace_fields']['enabled'] && $form_settings['email']['replace_fields']['use_array']){
			if(is_array($form_settings['email']['replace_fields']['custom_array'])) $_POST2 = $form_settings['email']['replace_fields']['custom_array'];
			$html="";
			foreach($_POST2 as $field => $value) {
				if(!in_array($field, $form_settings['fields_to_ignore'])) {
					if($field==$form_settings['file'] && $value!=""){
						$html .= '<b>'. ucwords(str_replace('_', ' ', $field)) . ':</b> ' . '<a href="'.BASE_URL_UPLOAD .'/' .$value .'">Image Link</a><br />';
					}else if ($value!=""){
						$html .= '<b>'. ucwords(str_replace('_', ' ', $field)) . ':</b> ' . $value . '<br />';
					}
				}
			}
			$array_mapping=array('html_content'=>$html);
			$form_settings['email']['body']=$this->replaceMappings($form_settings['email']['body'], $array_mapping);
		}


		//replace mappings email confirm
		if($form_settings['email_confirmation']['replace_fields']['enabled'] && !$form_settings['email_confirmation']['replace_fields']['use_array']){
			$form_settings['email_confirmation']['body']=$this->replaceMappings($form_settings['email_confirmation']['body'], $form_settings['email_confirmation']['replace_fields']['array_mapping']);
		}else if($form_settings['email_confirmation']['replace_fields']['enabled'] && $form_settings['email_confirmation']['replace_fields']['use_array']){
			if(is_array($form_settings['email_confirmation']['replace_fields']['custom_array'])) $_POST2 = $form_settings['email_confirmation']['replace_fields']['custom_array'];
			$html="";
			foreach($_POST2 as $field => $value) {
				if(!in_array($field, $form_settings['fields_to_ignore'])) {
					if($field==$form_settings['file'] && $value!=""){
						$html .= '<b>'. ucwords(str_replace('_', ' ', $field)) . ':</b> ' . '<a href="'.BASE_URL_UPLOAD .'/' .$value .'">Image Link</a><br />';
					}else if ($value!=""){
						$html .= '<b>'. ucwords(str_replace('_', ' ', $field)) . ':</b> ' . $value . '<br />';
					}
				}
			}
			$array_mapping=array('html_content'=>$html);
			$form_settings['email_confirmation']['body']=$this->replaceMappings($form_settings['email_confirmation']['body'], $array_mapping);
		}

		//return true if mail is disabled
		if(!$form_settings['email']['send_email']) {
			$success = array(
				'status' => true,
				'data'  => $data,
			);
			$success=array_merge($success, array('data'=>array('id'=>$insert_id, 'form_settings'=>$form_settings)));
			return $success;
		}

		//send mail api

		$_POST2 = $this->stripslashes_array($_POST2);

		if(MAIL_SERVICE=="MANDRILL"){
			$response=$this->MANDRILL->sendEmail($_POST2, $form_settings);
		}else if(MAIL_SERVICE=="MAILGUN"){
			$response=$this->MAILGUN->sendEmail($_POST2, $form_settings);
		}

		$response=array_merge($response, array('data'=>array('id'=>$insert_id, 'form_settings'=>$form_settings)));
		return $response;
	}

	public function sendSimpleEmail($from, $to, $subject, $body){
		$_POST2 = "";

		if($from==""){
			$from="noreply@".str_replace("www.", "", $_SERVER['SERVER_NAME']);
		}


		$form_settings = array(
			'required_fields' => array(),//only if not using jquery handler
			'fields_to_ignore' => array('imageField_x', 'imageField_y', 'submit', 'x', 'y', 'submit_x', 'submit_y', 'g-recaptcha-response'),
			'honeypot' => array(
				'enabled'=>false,
				'field_name' => 'subject',
			),
			'captcha' => false,
			'files'=>array(
				'enabled' => false, // true if you are submitting a file
				'filenames' => array("image"), // array of name of the file fields
			),
			'database' => array(
				'enabled' => false, // enable database submission
				'enable_id' => false, // takes the id from the last inserted row and adds it to the array
				'query' => "",
			),
			'email' => array(
				'send_email' => true, // true / false
				'from_email' => $from, // Email address it will show as sent from
				'from_name' => $from, // This will be shown as the name from the sender
				'to_email' => array($to), // Array of email addresses the form will be sent to (null will skip this section of the email submission)
				'cc_email' => NULL, // Array of email addresses the form will be cc to
				'bcc_email' => NULL, // Array of email addresses the form will be bcc to
				'subject' => $subject, // Subject of the email
				'replace_fields' => array(// Replaces preset fields with new values in an emailer//use body needs to be disabled//and values have to be defined before
					'enabled' => false,
					'array_mapping' => $array_mapping,
					'use_array'=>false, // set true to pop a POST to {html_content}
					'custom_array'=>NULL, // allows a different array than default $_POST2
				),
				'use_default_array' => array(
					'enabled' =>false, //enable this to use the premade array
					'text' => 'The following information has been submitted: <br /><br />', //add some pretext if you want. leave blank to add nothing before array
				),
				'use_custom_body' => array(
					'enabled' => true, // enable if using same body as main confirmation email
					'text' => $body,//file_get_contents(dirname(__FILE__) . '/../../portal/email/email.php'),
				),
			),
			'email_confirmation' => array(
				'send_email' => false, // true / false
				'from_email' => "noreply@".str_replace("www.", "", $_SERVER['SERVER_NAME']), // Email address it will show as sent from
				'from_name' => "noreply@".str_replace("www.", "", $_SERVER['SERVER_NAME']), // This will be shown as the name from the sender
				'to_email' => array($_POST2['email']), // Array of email addresses the form will be sent to
				'cc_email' => NULL, // Array of email addresses the form will be cc to
				'bcc_email' => NULL, // Array of email addresses the form will be bcc to
				'subject' => "Confirmation for ".$_POST2['first_name'], // Subject of the email
				'replace_fields' => array(// Replaces preset fields with new values in an emailer//use body needs to be disabled//and values have to be defined before
					'enabled' => false,
					'array_mapping' => $array_mapping,
					'use_array'=>false, // set true to pop a POST to {html_content}
					'custom_array'=>NULL, // allows a different array than default $_POST2
				),
				'use_default_array' => array(
					'enabled' =>false, //enable this to use the premade array
					'text' => 'The following information has been submitted: <br /><br />', //add some pretext if you want. leave blank to add nothing before array
				),
				'use_custom_body' => array(
					'enabled' => true, // enable if using same body as main confirmation email
					'text' => 'Thank you for your inquiry. We will contact you as soon as possible.',
				),
			),
		);

		if(MAIL_SERVICE=="MANDRILL"){
			$response=$this->MANDRILL->sendEmail($_POST2, $form_settings);
		}else if(MAIL_SERVICE=="MAILGUN"){
			$response=$this->MAILGUN->sendEmail($_POST2, $form_settings);
		}

		return $response;
	}

	function replaceMappings($body, $mappings){
		foreach($mappings as $key=>$value){
			$body=str_replace('{'.$key.'}', $value, $body);
		}
		return $body;
	}

	function getPaginationArray($array, $page, $show_per_page){
		$start = ($page -1) * ($show_per_page);
		$end = $show_per_page;
		$pages=count($all_posts)/$show_per_page;
		return array_slice($array, $start, $end);
	}

	function getPaginationDisplay($total_items, $show_per_page){
		$pages=$total_items/$show_per_page;
		$i = 1;
		while ($i <= $pages) {
			$display=$display.'<a href="?page='.$i.'">'.$i.'</a>&nbsp;';
			$i++;
        }
		return $display;
	}

	function print_r2($array, $display_all=true, $enable_exit=true) {
		if($display_all){
			echo '<pre>'; print_r($array); echo '</pre>';
		}else{
			if(in_array($_SERVER['REMOTE_ADDR'], $this->config_params['development_ips'])) {
				echo '<pre>'; print_r($array); echo '</pre>';
			}
		}

		if($enable_exit){
			exit;
		}
	}

	public function getDateDiff($date1, $date2=''){
		if($date2='')$date2=date('Y-m-d');
		$diff = abs(strtotime($date2) - strtotime($date1));
		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
		return array('years'=> $months,'months'=> $years,'days'=> $days);
	}

	public function arrayToIDKeys($array, $id_field = 'id') {
		$data = array();
		foreach($array as $key => $row) {
			$data[$row[$id_field]] = $row;
		}
		return $data;
	}

	public function curlGetURL($url, $info = array()){
		foreach($info as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string, '&');

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);

		$tmp = curl_exec($ch);
		curl_close($ch);
		if ($tmp != false){
			return $tmp;
		}
	}

	public function XMLtoArray($xml) {
		$array = json_decode(json_encode((array)simplexml_load_string($xml)),1);
		return $array;
	}

	public function hash_password($password) {
		$hashed_password = $this->salt1.$password.$this->salt2;
		$hashed_password = md5(md5($hashed_password));
		return $hashed_password;
	}

	public function humanFileSize($size,$unit="") {
		if( (!$unit && $size >= 1<<30) || $unit == "GB")
			return number_format($size/(1<<30),1)."GB";
		if( (!$unit && $size >= 1<<20) || $unit == "MB")
			return number_format($size/(1<<20),1)."MB";
		if( (!$unit && $size >= 1<<10) || $unit == "KB")
			return number_format($size/(1<<10),1)."KB";
		return number_format($size)." bytes";
	}

	public function removeSpecialChars($string) {
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
	   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}

	public function baseURL(){
		if(isset($_SERVER['HTTPS'])){
			$protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
		}
		else{
			$protocol = 'http';
		}
		return $protocol . "://" . $_SERVER['HTTP_HOST'];
	}

	public function uploadFile($file, $folder="", $use_upload_path=true){
		if($use_upload_path){
			$folder = BASE_SERVER_UPLOAD. '/'.$folder.'/';
		}else{
			$folder = BASE_SERVER. '/'.$folder.'/';
		}


		list($usec, $sec) = explode(" ", microtime());
		$file_name= round(((float)$usec + (float)$sec),0) . '_' . $file["name"];

		//$allowedExts = array("gif", "jpeg", "jpg", "png");
		$temp = explode(".", $file["name"]);
		$extension = end($temp);
		if ($file["error"] > 0) {
			return array("result" => false, "message"=>$file["error"] );
		}

		$file_destination=$folder. $file_name;
		if(move_uploaded_file($file["tmp_name"], $file_destination)) {
			//compress if it's an image... method checks for this
			$result = $this->compress_image($file_destination, $file_destination, 60);

    		return array("status" => true,
						"filename"=> $file_name,
						"type" => $file["type"],
						"size" => ($file["size"] / 1024) . " kB",
			);
    	}else{
			return array("result" => false, "message"=> "File could not be uploaded",);
		}
	}

	public function verifyPasswordComplexity($password, $uppercase=true, $lowercase=true, $numbers=true,$complexity=false, $length=8){
		$r1='/[A-Z]/';  //Uppercase
	    $r2='/[a-z]/';  //lowercase
	    $r3='/[!@#$%^&*()-_=+{};:,<.>]/';  // whatever you mean by 'special char'
	    $r4='/[0-9]/';  //numbers

	    if($uppercase){
			if (!preg_match($r1, $password)) {
				return array("result" => false, "message"=> "Password doesn't contain uppercase letters");
			}
		}

		if($lowercase){
			if (!preg_match($r2, $password)) {
				return array("result" => false, "message"=> "Password doesn't contain lowercase letters");
			}
		}

		if($numbers){
			if (!preg_match($r3, $password)) {
				return array("result" => false, "message"=> "Password doesn't contain numbers");
			}
		}

		if($complexity){
			if (!preg_match($r4, $password)) {
				return array("result" => false, "message"=> "Password doesn't contain complex characters");
			}
		}

		if(strlen($password)<$length){
			return array("result" => false, "message"=> "Password is not long enough");
		}
		return array("result" => true);
	}

	public function createNewPassword() {
		$newpass = rand().rand();
		return $newpass;
	}

	public function array_reorder_keys(&$array, $keynames){
	    if(empty($array) || !is_array($array) || empty($keynames)) return;
	    if(!is_array($keynames)) $keynames = explode(',',$keynames);
	    if(!empty($keynames)) $keynames = array_reverse($keynames);
	    foreach($keynames as $n){
	        if(array_key_exists($n, $array)){
	            $newarray = array($n=>$array[$n]); //copy the node before unsetting
	            unset($array[$n]); //remove the node
	            $array = $newarray + array_filter($array); //combine copy with filtered array
	        }
	    }
	}

	public function week_number($date){
	    return ceil(substr($date, -2) / 7);
	}

	public function addOrdinalNumberSuffix($num) {
	    if (!in_array(($num % 100),array(11,12,13))){
	      switch ($num % 10) {
	        // Handle 1st, 2nd, 3rd
	        case 1:  return $num.'st';
	        case 2:  return $num.'nd';
	        case 3:  return $num.'rd';
	      }
	    }
		return $num.'th';
	}

	 public function getHexKey($length) {
		$max = ceil($length / 32);
		$random = '';
		for ($i = 0; $i < $max; $i ++) {
		  $random .= md5(microtime(true).mt_rand(10000,90000));
		}
		return substr($random, 0, $length);
	}

	public function move_to_top(&$array, $key) {
	    $temp = array($key => $array[$key]);
	    unset($array[$key]);
		$array = $temp + $array;
	}

	public function move_to_bottom(&$array, $key) {
	    $value = $array[$key];
	    unset($array[$key]);
	    $array[$key] = $value;
	}

	public function cleanWindowsString($str){
		$win1252ToPlainTextArray = array(
		chr(130) => ',',
		chr(131) => '',
		chr(132) => ',,',
		chr(133) => '...',
		chr(134) => '+',
		chr(135) => '',
		chr(139) => '<',
		chr(145) => '\'',
		chr(146) => '\'',
		chr(147) => '"',
		chr(148) => '"',
		chr(149) => '*',
		chr(150) => '-',
		chr(151) => '-',
		chr(155) => '>',
		chr(160) => ' ',
		);
		$str = strtr($str, $win1252ToPlainTextArray);
		$str = trim($str);
		$patterns = array('%[\x7F-\x81]%', '%[\x83]%', '%[\x87-\x8A]%',
		'%[\x8C-\x90]%', '%[\x98-\xff]%');

		return preg_replace($patterns, '', $str); //Strip
	}



	 public function trackBreadCrumb($name, $reset=false){
		 $url = HTTP_HTTPS.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

		 //reset breadcrumbs if needed
		 if($reset){
			 unset($_SESSION['BREADCRUMBS']);
		 }

		 //see if url is in array and remove that and everyting after it
		 $remove=false;
		 foreach($_SESSION['BREADCRUMBS'] as $key=>$value){
		 	$url_test=explode('?', $url);
		 	$url_test2=explode('?', $value['url']);

			 if($url_test[0]==$url_test2[0]){
				 $remove=true;
			 }
			 if($remove){
				 unset($_SESSION['BREADCRUMBS'][$key]);
			 }
		 }

		 //Get highest index key
		if(is_array($_SESSION['BREADCRUMBS']) && count($_SESSION['BREADCRUMBS'])>0){
			$value = max(array_keys($_SESSION['BREADCRUMBS']));
		 }else{
			 $value=0;
		 }

		 $value++;

		 //set breadcrumbs values
		 $_SESSION['BREADCRUMBS'][$value]['url']=$url;
		 $_SESSION['BREADCRUMBS'][$value]['name']=$name;
	 }

	 public function getBreadCrumbString(){
		 $string="";
		 $count=0;
		 $breadcrumb_count=count($_SESSION['BREADCRUMBS']);

		 foreach($_SESSION['BREADCRUMBS'] as $key=>$value){
			 $count++;
			 if($breadcrumb_count==$count){
				$string .=$value['name'];
			 }else{
				 $string .='<a href="'.$value['url'].'">'.$value['name'].'</a> - ';
			 }
		 }
		 return $string;
	 }

	 public function simplifyMultiFileArray($files = array()){
		$sFiles = array();
		if(is_array($files) && count($files) > 0)
		{
			foreach($files as $key => $file)
			{
				foreach($file as $index => $attr)
				{
					$sFiles[$index][$key] = $attr;
				}
			}
		}
		return $sFiles;
	}

	public function addslashes_array($array){
		// Check if the parameter is an array
		if(is_array($array)) {
			// Loop through the initial dimension
			foreach($array as $key => $value) {
				// Check if any nodes are arrays themselves
				if(is_array($array[$key]))
					// If they are, let the function call itself over that particular node
					$array[$key] = $this->addslashes_array($array[$key]);

				// Check if the nodes are strings
				if(is_string($array[$key]))
					// If they are, perform the real escape function over the selected node
					$array[$key] = addslashes($array[$key]);
			}
		}
		// Check if the parameter is a string
		if(is_string($array))
			// If it is, perform a  mysql_real_escape_string on the parameter
			$array = addslashes($array);

		// Return the filtered result

		return $array;
	}

	public function stripslashes_array($array, $htmlentities=false){
		// Check if the parameter is an array
		if(is_array($array)) {
			// Loop through the initial dimension
			foreach($array as $key => $value) {
				// Check if any nodes are arrays themselves
				if(is_array($array[$key]))
					// If they are, let the function call itself over that particular node
					$array[$key] = $this->stripslashes_array($array[$key]);

				// Check if the nodes are strings
				if(is_string($array[$key]))
					// If they are, perform the real escape function over the selected node
					$array[$key] = stripslashes($array[$key]);
					if($htmlentities) $array[$key] = htmlentities($array[$key]);
			}
		}
		// Check if the parameter is a string
		if(is_string($array))
			// If it is, perform a  mysql_real_escape_string on the parameter
			$array = stripslashes($array);
			if($htmlentities) $array[$key] = htmlentities($array[$key]);

		// Return the filtered result

		return $array;
	}

	public function validate_vin($vin) {
		$vin = strtolower($vin);
		if (!preg_match('/^[^\Wioq]{17}$/', $vin)) {
			return false;
		}

		$weights = array(8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2);

		$transliterations = array(
			"a" => 1, "b" => 2, "c" => 3, "d" => 4,
			"e" => 5, "f" => 6, "g" => 7, "h" => 8,
			"j" => 1, "k" => 2, "l" => 3, "m" => 4,
			"n" => 5, "p" => 7, "r" => 9, "s" => 2,
			"t" => 3, "u" => 4, "v" => 5, "w" => 6,
			"x" => 7, "y" => 8, "z" => 9
		);

		$sum = 0;

		for($i = 0 ; $i < strlen($vin) ; $i++ ) { // loop through characters of VIN
			// add transliterations * weight of their positions to get the sum
			if(!is_numeric($vin{$i})) {
				$sum += $transliterations[$vin{$i}] * $weights[$i];
			} else {
				$sum += $vin{$i} * $weights[$i];
			}
		}

		// find checkdigit by taking the mod of the sum

		$checkdigit = $sum % 11;

		if($checkdigit == 10) { // checkdigit of 10 is represented by "X"
			$checkdigit = "x";
		}

		return ($checkdigit == $vin{8});
	}


	public function array_column( array $input, $column_key, $index_key = null ) {
        $result = array();
        foreach( $input as $k => $v )
            $result[ $index_key ? $v[ $index_key ] : $k ] = $v[ $column_key ];

        return $result;
    }

	public function getCMSUrl(){
		$script_parts2 =  explode('?',$_SERVER["REQUEST_URI"]);

		$url = HTTP_HTTPS.$_SERVER['SERVER_NAME'].$script_parts2[0];
		return rtrim(ltrim($url));
	}

	public function convert_smart_quotes($string) {
        $search = array(chr(145),
                        chr(146),
                        chr(147),
                        chr(148),
                        chr(151));

       $replace = array("'",
                     "'",
                     '"',
                     '"',
                     '-');

        return str_replace($search, $replace, $string);
    }

	public function getAgeFromBirthday( $birthday ) {
	    $birthday = date("m/d/Y", $birthday);
		$age = date_diff(date_create($birthday), date_create('now'))->y;
		return $age;
    }

	public function createHexKey(){
		$key = md5(rand());
		return $key;
	}



	public function arrayDiff($old_items, $new_items){
		if(is_array($new_items) && is_array($old_items)){
			if(count(array_diff($new_items, $old_items))>0){
				$is_new_items=true;
			}

			$add_items = array_diff($new_items, $old_items);
			$remove_items = array_diff($old_items, $new_items);
		}else if(!is_array($new_items)){
			//nothing else can be added so everything that was approved needs to be removed
			$remove_items = $old_items;
		}

		$array_response['add_items']=$add_items;
		$array_response['remove_items']=$remove_items;
		$array_response['new_items']=$is_new_items;
		return $array_response;
	}




	public function queryBuilder($_POST2, $allowed_fields){
		//remove fields not allowed
		foreach($_POST2 as $key => $value){
			if(!in_array($key, $allowed_fields)){
				unset($_POST2[$key]);
			}
		}

		$num_fields = count($_POST2);
        $query = "";

        $count = 1;

        foreach($_POST2 as $field => $value) {
            $query .= "`".$field."` = '".$value."'";
            if($count != $num_fields) {
                $query .= ", ";
            }
            $count++;
        }

		return $query;
	}

	public function compress_image($src, $dest , $quality){
		$info = getimagesize($src);

		if ($info['mime'] == 'image/jpeg'){
			$image = imagecreatefromjpeg($src);
		}elseif ($info['mime'] == 'image/gif'){
			$image = imagecreatefromgif($src);
		}elseif ($info['mime'] == 'image/png'){
			$image = imagecreatefrompng($src);
		}else{
			return $src;
		}

		//compress and save file to jpg
		imagejpeg($image, $dest, $quality);

		//return destination file
		return $dest;
	}



	public function santizeGET(){
		foreach($_GET as $key=>$value){
			$_GET[$key] = preg_replace('/[^-a-zA-Z0-9_ ,\/]/', '', $_GET[$key]);
			$_GET[$key]=strip_tags($_GET[$key]); //just an example filter
			$_GET[$key]=htmlspecialchars($_GET[$key]); //just an example filter
		}
	}

	public function requiredFields($required_fields, $_POST2){
		foreach($required_fields as $key => $field) {
				if($_POST2[$field]=="") {
				$error = array(
					'status' => false,
					'message' => 'Please fill in all the required fields.'
				);
				return $error;
			}
		}

	}



	public function startsWith($haystack, $needle) {
		// search backwards starting from haystack length characters from the end
		return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
	}

	public function endsWith($haystack, $needle) {
		// search forward starting from end minus needle length characters
		return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
	}

	public function deleteFolder($dir) {
		$files = array_diff(scandir($dir), array('.','..'));
		foreach ($files as $file) {
		   (is_dir("$dir/$file")) ? $this->deleteFolder("$dir/$file") : unlink("$dir/$file");
		}
		return rmdir($dir);
	}

	public function array_unique_key($array, $key) {
		$temp_array = array();
		$i = 0;
		$key_array = array();

		foreach($array as $val) {
			if (!in_array($val[$key], $key_array)) {
				$key_array[$i] = $val[$key];
				$temp_array[$i] = $val;
			}
			$i++;
		}
		return $temp_array;
	}

	public function phpinfo2array() {
		$entitiesToUtf8 = function($input) {
			// http://php.net/manual/en/function.html-entity-decode.php#104617
			return preg_replace_callback("/(&#[0-9]+;)/", function($m) { return mb_convert_encoding($m[1], "UTF-8", "HTML-ENTITIES"); }, $input);
		};
		$plainText = function($input) use ($entitiesToUtf8) {
			return trim(html_entity_decode($entitiesToUtf8(strip_tags($input))));
		};
		$titlePlainText = function($input) use ($plainText) {
			return '# '.$plainText($input);
		};

		ob_start();
		phpinfo(-1);

		$phpinfo = array('phpinfo' => array());

		// Strip everything after the <h1>Configuration</h1> tag (other h1's)
		if (!preg_match('#(.*<h1[^>]*>\s*Configuration.*)<h1#s', ob_get_clean(), $matches)) {
			return array();
		}

		$input = $matches[1];
		$matches = array();

		if(preg_match_all(
			'#(?:<h2.*?>(?:<a.*?>)?(.*?)(?:<\/a>)?<\/h2>)|'.
			'(?:<tr.*?><t[hd].*?>(.*?)\s*</t[hd]>(?:<t[hd].*?>(.*?)\s*</t[hd]>(?:<t[hd].*?>(.*?)\s*</t[hd]>)?)?</tr>)#s',
			$input,
			$matches,
			PREG_SET_ORDER
		)) {
			foreach ($matches as $match) {
				$fn = strpos($match[0], '<th') === false ? $plainText : $titlePlainText;
				if (strlen($match[1])) {
					$phpinfo[$match[1]] = array();
				} elseif (isset($match[3])) {
					$keys1 = array_keys($phpinfo);
					$phpinfo[end($keys1)][$fn($match[2])] = isset($match[4]) ? array($fn($match[3]), $fn($match[4])) : $fn($match[3]);
				} else {
					$keys1 = array_keys($phpinfo);
					$phpinfo[end($keys1)][] = $fn($match[2]);
				}

			}
		}

		return $phpinfo;
	}

}
?>