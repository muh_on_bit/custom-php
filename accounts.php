<?php

class Accounts extends Base {
	public $Modules;

	public $user_fields = array(
		'id', 'first_name', 'last_name', 'email', 'password', 'created', 'status',
	);

	public $required_fields = array(
		'first_name',
		'last_name',
		'email',
		'email2',
		'password',
		'password2',
	);

	public $account_verification_page = 'verify.php';
	public $account_verification_page_full = 'verify-full.php';
	public $user_homepage = 'user-dashboard.php';

	function __construct() {
	   parent::__construct($config_params);
	   		$this->Modules = new Modules();

	}

	public function checkAccountSession($public_pages,$account_types) {
		$script_path=$_SERVER["REQUEST_URI"];

		$script_parts =  explode('/',$_SERVER["REQUEST_URI"]);
		array_pop($script_parts);
		$last =implode('/', $script_parts);
		if(!$last) $last ="/".$last; //match the public_pages

		$script_name =  $_SERVER["SCRIPT_NAME"];

		if(!in_array($script_path, $public_pages) && !in_array($last, $public_pages) && !in_array($script_name, $public_pages)) {
			if(!$_SESSION['SETTINGS']['account_logged_in']) {
				if(pathinfo($script_path, PATHINFO_EXTENSION)=="") $_SESSION['SETTINGS']['redirect_url'] = $script_path;

				if($script_path){
					$this->redirect(BASE_URL_PORTAL. '/Login/Login');
				}else{
					$this->redirect(BASE_URL. '/Login/Login');
				}

			}else{

				if($_SESSION['ACCOUNT']['type']!="Super Admin"){

					$modules = $this->Modules->getAllModulePermissionsForCurrentUser();

					$account_pages=array();

					foreach($modules as $module_key => $module){
						$module['location_url'] = BASE_URL.$module['location_url'];

						array_push($account_pages, $module['location_url']);
					}

					$base_url=$script_parts;
					unset($base_url[count($base_url)-1]);
					$base_url = implode("/",$base_url);

					$url = parent::getCMSUrl();

					if(!in_array($_SERVER["SCRIPT_NAME"], $account_pages) && !in_array($base_url, $account_pages) && !in_array($url, $account_pages)) {

						$this->redirect(BASE_URL_PORTAL. '/Dashboard/NoAccess');

						exit;
					}
				}
			}
		}

		//set last page url in session
		$_SESSION['SETTINGS']['last_page'] = $this->currentURL($_GET);
	}

	public function getAllAccounts(){
		$query = "SELECT
					a.*,
					at.name AS type
				FROM `accounts` a

				LEFT JOIN
					`accounts_types` at
				ON `at`.`id` = `a`.`accounts_types_id`";
		$data = $this->DB->fetchAll($query);

		return $data;
	}

	public function getAllTypes(){
		$query = "SELECT * FROM `accounts_types`";
		$data = $this->DB->fetchAll($query);
		return $data;
	}



	public function updateLastActive(){
		if(isset($_SESSION['ACCOUNT']['id'])){
			$query = "UPDATE `accounts` SET
				last_active='".date("Y-m-s H:i:s")."'
				WHERE
					id='".$_SESSION['ACCOUNT']['id']."'
			";

			$result=$this->DB->query($query);
		}
	}


	public function getAccountByID($id){
		$query = "SELECT
						accounts.*,
						at.name as type
					FROM
						`accounts`
					LEFT JOIN
						`accounts_types` at
					ON at.id=accounts.accounts_types_id
					WHERE accounts.`id` = '".$id."'";

		$data = $this->DB->fetchAll($query);
		return array_shift($data);
	}

	public function getTypeByID($id){
		$query = "SELECT * FROM `accounts_types` WHERE `id` = '".$id."'";
		$data = $this->DB->fetchAll($query, $autoshift=true);

		$role_access= $this->getRoleAccessById($id);

		$access_ids = array();
		foreach($role_access as $key=>$value){
			array_push($access_ids, $value['accounts_roles_id']);
		}

		$data['roles_access']=$access_ids;

		return $data;
	}


	 public function getRoleAccessById($id){
		$query = "SELECT * FROM `accounts_types_roles` WHERE `accounts_types_id` = '".$id."'";
		$data = $this->DB->fetchAll($query, $autoshift=false);
		return $data;
	}

	public function getAdminByEmail($email){
		$query = "SELECT * FROM `accounts` WHERE `email` = '".$email."'";
		$data = $this->DB->fetchAll($query);
		return $data;
	}

	public function addAccount($_POST2){
		if($this->emailExists($_POST2['email'])) {
			return array("status" => false, "message"=>"Email already exists.");
		}

		if($this->usernameExists($_POST2['username'])) {
			return array("status" => false, "message"=>"Username already exists.");
		}

		//Submit file
		if($_FILES2['avatar']['size'] > 1000 && $_FILES2['avatar']){
			$avatar=$this->uploadProfilePhoto($_FILES2['avatar']);
		}

		$accounts=$this->getAdminByEmail($_POST2['email']);
		if(count($accounts)>0){
			return array("status" => false, "message"=>"This email is already taken! Please choose another email");
		}

		//validate password method:
		if($_POST2['password']!= $_POST2['password2']) {
			return array("status" => false, "message"=>"Password do not match");
		}

		if($_POST2['password']=="" && $_POST2['password2']=="") {
			return array("status" => false, "message"=>"Please type in a password");
		}


		$query = "INSERT INTO `accounts` SET
			datetime='".date("Y-m-d H:i:s")."',
			first_name='".$_POST2['first_name']."',
			last_name='".$_POST2['last_name']."',
			email='".$_POST2['email']."',
			username='".$_POST2['username']."',
			avatar='".$_POST2['avatar']."',
			accounts_types_id='".$_POST2['accounts_types_id']."',
			status='".$_POST2['status']."'";

		if($_POST2['password']!="" && $_POST2['password2']!=""){
			$query.=", password='".parent::hash_password($_POST2['password'])."'";
		}

		$result=$this->DB->query($query);

		if($result['status']) {
			return array("status" => true, "message"=>"Account Added successfully");
		} else {
			return array("status" => false, "message"=>"Account could not be added");
		}

	}


	public function addType($_POST2){


		$query = "INSERT INTO `accounts_types` SET
			name='".$_POST2['name']."',
			dashboard_url='".$_POST2['dashboard_url']."'";


		$result_roles=$this->DB->query($query);
		$type_id= $result_roles['id'];

		$query = "DELETE FROM `accounts_types_roles` WHERE `accounts_types_id` ='".$_POST2['id']."'";

		$result=$this->DB->query($query);

		if(count($_POST2['roles_access'])>0){
			foreach($_POST2['roles_access'] as $value){
				$query = "INSERT INTO `accounts_types_roles`
							SET
								`accounts_roles_id` ='".$value."',
								`accounts_types_id` ='".$type_id."'
							";

				$result=$this->DB->query($query);

			}
		}

		if($result_roles['status']) {
			return array("status" => true, "message"=>"Account Type Added successfully");
		} else {
			return array("status" => false, "message"=>"Account Type could not be added");
		}

	}


	public function editType($_POST2){
		if($_POST2['delete'] == 1) {

			$query = "DELETE FROM `accounts_types_roles` WHERE `accounts_types_id` ='".$_POST2['id']."'";

			$result=$this->DB->query($query);

			$query = "DELETE FROM `accounts_types` WHERE `id` = '".$_POST2['id']."'";
			$result=$this->DB->query($query);

			if($result['status']) {
				return array("status" => true, "message"=>"Account type deleted successfully");
			} else {
				return array("status" => false, "message"=>"Account type could not be deleted");
			}
		}

		$query = "UPDATE `accounts_types` SET
				name='".$_POST2['name']."',
					dashboard_url='".$_POST2['dashboard_url']."'
				WHERE
					id='".$_POST2['id']."'
			";

		$result_type=$this->DB->query($query);

		$query = "DELETE FROM `accounts_types_roles` WHERE `accounts_types_id` ='".$_POST2['id']."'";

		$result=$this->DB->query($query);

		if(count($_POST2['roles_access'])>0){
			foreach($_POST2['roles_access'] as $value){
				$query = "INSERT INTO `accounts_types_roles`
							SET
								`accounts_roles_id` ='".$value."',
								`accounts_types_id` ='".$_POST2['id']."'
							";

				$result=$this->DB->query($query);
			}
		}

		if($result_type['status']) {
			return array("status" => true, "message"=>"Account Type Updated successfully");
		} else {
			return array("status" => false, "message"=>"Account Type could not be Updated");
		}

	}

	public function getRoles(){
		$query = "SELECT * FROM `accounts_roles`";
		$data = $this->DB->fetchAll($query);
		return $data;
	}

	public function editAccount($_POST2){
		if($_POST2['delete'] == 1) {
			$query = "DELETE FROM `accounts` WHERE `id` = '".$_POST2['id']."'";
			$result=$this->DB->query($query);

			if($result['status']) {
				return array("status" => true, "message"=>"Account deleted successfully");
			} else {
				return array("status" => false, "message"=>"Account could not be deleted");
			}
		}


		if($this->emailExistsByUser($_POST2['email'], $_POST2['id'])) {
			return array("status" => false, "message"=>"Email already exists.");
		}

		if($this->usernameExistsByUser($_POST2['username'], $_POST2['id'])) {
			return array("status" => false, "message"=>"Username already exists.");
		}


		//validate password method:
		if($_POST2['password']!= $_POST2['password2']) {
			return array("status" => false, "message"=>"Password do not match");
		}

		//Submit file
		if($_FILES2['avatar']['size'] > 1000 && $_FILES2['avatar']){
			$avatar=$this->uploadProfilePhoto($_FILES2['avatar']);
		}

		$query = "UPDATE `accounts` SET
				first_name='".$_POST2['first_name']."',
				last_name='".$_POST2['last_name']."',
				email='".$_POST2['email']."',
				username='".$_POST2['username']."',
				avatar='".$_POST2['avatar']."',
				accounts_types_id='".$_POST2['accounts_types_id']."',
			";
		if($_POST2['password'] <> "") {
			$query .= "
			password='".parent::hash_password($_POST2['password'])."', ";
		}
		$query .= "
			status='".$_POST2['status']."'
		WHERE
			id='".$_POST2['id']."'";

		$result=$this->DB->query($query);


		if($result['status']) {
			return array("status" => true, "message"=>"Account was updated successfully");
		} else {
			return array("status" => false, "message"=>"Account could not be updated");
		}

	}


	public function userHasAccessForRole($role){
		if($_SESSION['ACCOUNT']['type']=="Super Admin") return true;

		$role_type_id = $_SESSION['ACCOUNT']['accounts_types_id'];

		$query="SELECT
					at.name AS accounts_types_name,
					ar.id,
					ar.name,
					ar.shortname
				FROM
					`accounts_types_roles` atr
				INNER JOIN `accounts_types` at
					ON at.id = atr.accounts_types_id
				INNER JOIN `accounts_roles` ar
					ON ar.id = atr.accounts_roles_id
				WHERE
					ar.shortname='".$role."'
				AND
					at.id='".$role_type_id."'

		";

		$data = $this->DB->fetchAll($query);

		if(count($data)>0) return true;

		return false;
	}


	public function changePassword($_POST2){
		if($_POST2['password'] != $_POST2['password2']){
			return array("status" => false, "message"=>"Please enter the password twice the same time");
		}

		$query = "
				SELECT
					*
				FROM
					`accounts`
				WHERE
					`accounts`.`id` = '".$_POST2['id']."'
					AND
					`accounts`.`password` = '".parent::hash_password($_POST2['old-password'])."'
				LIMIT 1
				";

		$data = $this->DB->fetchAll($query);

		foreach($data as $row) {
			$admin_id = $row['id'];
		}

		if(isset($admin_id) && $admin_id <> '') {
		} else {
			return array("status" => false, "message"=>"Could not find current user");
		}

		$query = "UPDATE `accounts` SET
					password='".parent::hash_password($_POST2['password'])."',
					status='".$_POST2['status']."'
				WHERE
					id='".$_POST2['id']."'";
		$result=$this->DB->query($query);

		if($result['status']) {
			return array("status" => true, "message"=>"Password updated successfully");
		} else {
			return array("status" => false, "message"=>"Could not update password");
		}

	}

	public function logoutAccount($_POST2, $redirect=false,$account_types=array()) {
		session_unset();
		session_destroy();
		return array("status" => true, "message"=>"You have successfully logged out of the system.");
	}



	public function emailExistsByUser($email, $account_id) {
		$query = "SELECT * FROM `accounts` WHERE `email` = '".$email."' AND `id`<>'".$account_id."'";
		$data = $this->DB->fetchAll($query);

		if(count($data) > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function usernameExistsByUser($username, $account_id) {
		$query = "SELECT * FROM `accounts` WHERE `username` = '".$username."' AND `id`<>'".$account_id."'";
		$data = $this->DB->fetchAll($query);

		if(count($data) > 0) {
			return true;
		} else {
			return false;
		}
	}


	public function loginAccount($_POST2, $redirect=false,$account_types=array()) {
		$email=$_POST2['email'];//username or email passes on this field
		$password =$_POST2['password'];
		$remember_me=$_POST2['remember_me'];

		$query = "SELECT
						`a`.*,
						`at`.`name` as type,
						`at`.`dashboard_url` as dashboard

					FROM
						`accounts` a

					LEFT JOIN
						`accounts_types` at
					ON `at`.`id` = `a`.`accounts_types_id`
					WHERE
						(`a`.`email` = '".$email."' OR `a`.`username` = '".$email."')

						AND
						`a`.`password` = '".parent::hash_password($password)."'
						AND `status` =1
		";

		$data = $this->DB->fetchAll($query, $autoshift=true);

		$admin_id = $data['id'];
		if(isset($admin_id) && $admin_id <> '') {
			$_SESSION['ACCOUNT']=$data;
			$_SESSION['SETTINGS']['account_logged_in'] = 1;

			//add times
			if($remember_me==1){
				$_SESSION['SETTINGS']['stay_logged_in']=1;
			}else{
				$_SESSION['SETTINGS']['stay_logged_in']=0;
			}

			if($redirect){
    			if($_SESSION['SETTINGS']['redirect_url'] && (in_array($_SESSION['SETTINGS']['redirect_url'],$account_types[$_SESSION['ACCOUNT']['type']]['pages']) || $_SESSION['ACCOUNT']['type']=='admin')) {
					$redirect_url = $_SESSION['SETTINGS']['redirect_url'];
					unset($_SESSION['SETTINGS']['redirect_url']);
					header("Location: ".$redirect_url);
				} else {
					// redirect to dashboard
					header("Location: ".BASE_URL.$_SESSION['ACCOUNT']['dashboard']);
				}
			}

			return array("status" => true, "message"=>"Login successful", "data"=>$admin_id);
		} else {
			return array("status" => false, "message"=>"Login failed, please try again.");
		}

	}

	public function emailExists($email) {
		$query = "SELECT * FROM `accounts` WHERE `email` = '".$email."'";
		$data = $this->DB->fetchAll($query);

		if(count($data) > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function usernameExists($username) {
		$query = "SELECT * FROM `accounts` WHERE `username` = '".$username."'";
		$data = $this->DB->fetchAll($query);

		if(count($data) > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function logout() {
		unset($_SESSION['ACCOUNT']);
		unset($_SESSION['SETTINGS']);
		unset($_SESSION);
		session_destroy();

	}

	public function createNewPassword() {
		$newpass = rand().rand();
		return $newpass;
	}

	/***FOR API, PENDING FULL INTEGRATION***/

	// $identifier is the ID or email address, $type = 'id' or 'email'
	public function getUserByIdentifier($identifier, $type = 'id') {
		$dat = array();
		$query = "SELECT " . implode(', ', $this->user_fields) . " FROM `accounts` WHERE `" . $type . "` = '" . $identifier . "'";
		$data = $this->DB->fetchAll($query);

		if($data) {
			return array_shift($data);
		} else {
			return "error";
		}
	}

	public function editUser($user_id, $data) {
		if($data['password']) {
			$data['password'] = parent::hash_password($data['password']);
		}
		$query_params = array();
		foreach ($data as $field => $value) {
			array_push($query_params, " `" . $field . "` = '" . $value . "'");
		}
		$query_params_string = implode(', ', $query_params);
		$query = "UPDATE `accounts` SET " . $query_params_string . " WHERE `id` = '" . $user_id . "'";
		$result=$this->DB->query($query);

		if($result['status']) {
			return true;
		} else {
			return false;
		}
	}


	/*****************************
	*** API Stuff
	*****************************/


	public function registerUserAccount($_POST2){

		//Submit file
		if($_FILES2['avatar']['size'] > 1000 && $_FILES2['avatar']){
			$avatar=$this->uploadProfilePhoto($_FILES2['avatar']);
		}

		//Check required fields
		foreach($this->required_fields as $key => $field) {
			if($_POST2[$field]=="") {
				$error = array(
					'result' => "error",
					'message' => 'Please complete all the required fields.'
				);
				return $error;
			}
		}

		//check if emails match
		if($_POST2['email']!=$_POST2['email2']) {
			$error = array(
				'result' => "error",
				'message' => 'Please make sure emails are the same.'
			);
			return $error;
		}

		//check if passwords match
		if($_POST2['password']!=$_POST2['password2']) {
			$error = array(
				'result' => "error",
				'message' => 'Please make sure passwords are the same.'
			);
			return $error;
		}

		//check if email already exists
		if($this->emailExists($_POST2['email'])) {
			$error = array(
				'result' => "error",
				'message' => 'This email is taken. Please use a different one.'
			);
			return $error;
		}

		//verify email is correct
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$error = array(
				'result' => "error",
				'message' => 'Invalid email format.'
			);
			return $error;
		}

		//database submit
		if($error) { // All required fields are completed
			return $error;
		}

		// Create password hash
		$_POST2['password']=parent::hash_password($_POST2['password']);

		$query_user = "INSERT INTO `accounts` SET
					datetime='".date("Y-m-d H:i:s")."',
					first_name='".$_POST2['first_name']."',
					last_name='".$_POST2['last_name']."',
					email='".$_POST2['email']."',
					avatar='".$_POST2['avatar']."',
					password='".$_POST2['password']."',
					type='user',
					status='2'
				";
		$result=$this->DB->query($query);

		$user_id = $result['id'];

		$_POST2['user_id'] = $user_id;
		$email_sent = $this->sendNewVerificationEmail($_POST2['email']);

		if($email_sent) {
			$array = array(
				'result' => "success",
				'message' => 'Your user account has been created.',
			);
		} else {
			$array = array(
				'result' => "error",
				'message' => 'There was an error sending your verification code.',
			);
		}

		return $array;
	}

	public function verifyCode($verify_code){
		$query = "SELECT user_id, created FROM `accounts_verify` WHERE `code` = '".$verify_code."' AND `status` = 'unverified' LIMIT 1";
		$data = $this->DB->fetchAll($query);

		if(count($data) == 0) {
			$error = array(
				'result' => 1,
				'message' => 'Verification Failed: Token not found.'
			);
			return $error;
		} else {
			$data = array_shift($data);
		}

		$exp_date = date('Y-m-d H:i:s', strtotime($data['created'] . ' + 24 hours'));

		if(date("Y-m-d H:i:s") >= $exp_date){
			$error = array(
				'result' => 1,
				'message' => 'Verification Failed: Invalid Token.'
			);
			return $error;
		}

		$query = "UPDATE `accounts_verify` SET
						status = 'verified'
				 WHERE `code` = '".$verify_code."'
					";

		$result=$this->DB->query($query);


		$query = "UPDATE `accounts` SET
						status='1'
					WHERE `id` = '".$data['user_id']."'
					";
		$result2=$this->DB->query($query);


		if($result['status'] && $result2['status']) {
			return "success";
		}else{
			$error = array(
					'result' => 1,
					'message' => 'Verification Failed: Database submission failed.'
				);
				return $error;
		}
	}

	public function uploadProfilePhoto($file){
		list($usec, $sec) = explode(" ", microtime());
		$file_name= round(((float)$usec + (float)$sec),0) . '_' . $file["name"];

		//$allowedExts = array("gif", "jpeg", "jpg", "png");
		$temp = explode(".", $file["name"]);
		$extension = end($temp);
		if ($file["error"] > 0) {
			return array("result" => false, "message"=>$file["error"] );
		}
		$write_file_location=BASE_SERVER_PORTAL. '/img/avatars/'. $file_name;
		if(move_uploaded_file($file["tmp_name"], $write_file_location)) {
    		return array("result" => true,
						"filename"=> $file_name,
						"type" => $file["type"],
						"size" => ($file["size"] / 1024) . " kB",
			);
    	}else{
			return array("result" => false, "message"=> "File could not be uploaded",'data'=>$file);
		}
	}

	public function submitLogin($_POST2){
		$query = "
		SELECT
			*
		FROM
			`accounts`
		WHERE
			`accounts`.`email` = '".$_POST2['email']."'
			AND
			`accounts`.`password` = '".parent::hash_password($_POST2['password'])."'
			AND `status` =1
		LIMIT 1
		";

		$data = $this->DB->fetchAll($query);

		if(count($data) > 0 && $data[0]['status'] == '1') {
			$this->setUserSession($data[0]);

			$data = array(
				'result' => "success",
				'data' => array_shift($data),
			);
		} else if($data[0]['status'] == '0' || $data[0]['status'] == '2') {
			$this->sendNewVerificationEmail($data[0]['email']);
			$data = array(
				'result' => "error",
				'message' => 'Your account is not yet verified. Please check your email/junk mail and verify your account.',
			);
		} else {
			$data = array(
				'result' => "error",
				'message' => 'Email address / password not found.',
			);
		}

		return $data;
	}

	public function setUserSession($user) {
		$_SESSION['user'] = $user;
	}

	public function createNewVerificationCode($user_id) {
		$verification_code = parent::create_password("CcX#CcX#CcX#CcX#CcX#CcX#");

		$query = "INSERT INTO `accounts_verify` SET
							created = '".date('Y-m-d H:i:s')."',
							user_id='".$user_id."',
							code='".$verification_code."',
							status='unverified'
						";
		$result=$this->DB->query($query);

		if($result['status']) {
			return $verification_code;
		} else {
			return "error";
		}
	}

	/*****************************
	*** Inputs: email
	*** Checks that email exists, then generates and emails a new verification code
	*****************************/
	public function sendNewVerificationEmail($email) {
		if($this->emailExists($email)) {
			$user = $this->getUserByIdentifier($email, 'email');
			if($user['status'] == '1') {
				$error = array(
					'result' => "error",
					'message' => 'User account is already active.',
				);
				return $error;
			} else {
				$verification_code = $this->createNewVerificationCode($user['id']);

				if($verification_code) {
					$form_settings = array('required_fields' => null, 'fields_to_ignore' => array('imageField_x', 'imageField_y', 'submit', 'x', 'y', 'submit_x', 'submit_y'), 'captcha_enabled' => "error", // mark as "success" if you want to check the captcha field
									'honeypot' => "error", // has to use subject textbox
									'submit_file' => "error", // "success" if you are submitting a file
									'file' => "image", // name of the file field
									'enable_id' => "error", // takes the id from the last inserted row and adds it to the array

									'email' => array('send_email' => "success", // "success" / "error"
									'from_email' => "test@hotmail.com", // Email address it will show as sent from
									'from_name' => "test@hotmail.com", // This will be shown as the name from the sender
									'to_email' => null, // Array of email addresses the form will be sent to (null will skip this section of the email submission)
									'to_name' => null, // Array of email addresses the form will be sent to (null will skip this section of the email submission)
									'subject' => "" // Subject of the email
									), 'email_confirmation' => array('send_email' => "success", // "success" / "error"
									'from_email' => "test@test.com", // Email address it will show as sent from
									'from_name' => "test@test.com", // This will be shown as the name from the sender
									'to_email' => array($user['email']), // Array of email addresses the form will be sent to
									'to_name' => array($user['first_name'] . ' ' . $user['last_name']), // Array of email addresses the form will be sent to
									'subject' => "test - Verify Your Account", // Subject of the email
									'replace_fields' => array(// Replaces preset fields with new values in an emailer//use body needs to be disabled//and values have to be defined before
									'enabled' => "success", 'array_mapping' => $array_mapping, ), 'use_same_body' => "error", // enable if using same body as main confirmation email
									'body' => 'http://git.domain.com/test/' . $this->account_verification_page . '?verify_code=' . $verification_code), 'database' => array('enabled' => "error", // enable database submission
									'query' => $query));
					
					return $this->MAIL->sendEmail($user,$form_settings);

				}
			}
		} else {
			$error = array(
				'result' => "error",
				'message' => 'User account not found.',
			);
			return $error;
		}
	}

	public function sendPasswordReset($user_id) {
		$new_password = parent::create_password('CcX#CcX#');

		//Save new password to client's account
		$data = array(
			'password' => $new_password,
		);
		$account_updated = $this->editUser($user_id, $data);

		if($account_updated) {
			$user = $this->getUserByIdentifier($user_id, 'id');
			$vars = array(
				'name' => $user['first_name'] . ' ' . $user['last_name'],
				'new_password' => $new_password,
				'login_link' => parent::baseURL() . '/login.php',
			);

			$form_settings = array('required_fields' => null, 'fields_to_ignore' => array('imageField_x', 'imageField_y', 'submit', 'x', 'y', 'submit_x', 'submit_y'), 'captcha_enabled' => "error", // mark as "success" if you want to check the captcha field
							'honeypot' => "error", // has to use subject textbox
							'submit_file' => "error", // "success" if you are submitting a file
							'file' => "image", // name of the file field
							'enable_id' => "error", // takes the id from the last inserted row and adds it to the array

							'email' => array('send_email' => "success", // "success" / "error"
							'from_email' => "test@hotmail.com", // Email address it will show as sent from
							'from_name' => "test@hotmail.com", // This will be shown as the name from the sender
							'to_email' => null, // Array of email addresses the form will be sent to (null will skip this section of the email submission)
							'to_name' => null, // Array of email addresses the form will be sent to (null will skip this section of the email submission)
							'subject' => "" // Subject of the email
							), 'email_confirmation' => array('send_email' => "success", // "success" / "error"
							'from_email' => "test@domain.com", // Email address it will show as sent from
							'from_name' => "test@domain.com", // This will be shown as the name from the sender
							'to_email' => array($user['email']), // Array of email addresses the form will be sent to
							'to_name' => array($user['first_name'] . ' ' . $user['last_name']), // Array of email addresses the form will be sent to
							'subject' => "test - Password Reset", // Subject of the email
							'replace_fields' => array(// Replaces preset fields with new values in an emailer//use body needs to be disabled//and values have to be defined before
							'enabled' => "error", 'array_mapping' => $array_mapping, ), 'use_same_body' => "success", // enable if using same body as main confirmation email
							'body' => 'http://domain.com/test/' . $this->account_verification_page . '?verify_code=' . $verification_code), 'database' => array('enabled' => "error", // enable database submission
							'query' => $query));
			return $this->MAIL->sendEmail($vars,$form_settings);
		}

	}

	public function currentUserID() {
		return $_SESSION['user']['id'];
	}

	public function getPermissionsForCurrentUser(){
		$query = "SELECT * FROM `accounts_permissions` AS ap
					INNER JOIN `permissions` AS p ON `ap`.`permissions_id` = `p`.`id`
					WHERE `ap`.`accounts_id` = '".$_SESSION['ACCOUNT']['id']."'
		";

		$data = $this->DB->fetchAll($query);

		$permissions=array();
		foreach($data as $key => $value){
			array_push($permissions, $value['name']);
		}

		return $permissions;
	}

	public function forgotPassword($_POST2) {
		$email = $_POST2['email'];
		$new_password = parent::create_password('CcX#CcX#');

		//Save new password to client's account
		$data = array(
			'password' => $new_password,
		);

		$account=$this->getAdminByEmail($email);

		if(count($account)==0){
			$error = array(
				'status' => false,
				'message' => 'Email for user account not found.',
			);
			return $error;

		}

		$account_updated = $this->editUser($account['id'], $data);

		if($account_updated) {
			$user = $this->getUserByIdentifier($account['id'], 'id');
			$vars = array(
				'name' => $user['first_name'] . ' ' . $user['last_name'],
				'new_password' => $new_password,
				'login_link' => parent::baseURL() . '/login.php',
			);


			$form_settings = array(
							'required_fields' => array(),//only if not using jquery handler
							'fields_to_ignore' => array('imageField_x', 'imageField_y', 'submit', 'x', 'y', 'submit_x', 'submit_y'),
							'honeypot' => true, // has to use subject textbox
							'submit_file' => false, // true if you are submitting a file
							'file' => "image", // name of the file field
							'enable_id' => false, // takes the id from the last inserted row and adds it to the array
							'database' => array(
								'enabled' => false, // enable database submission
								'query' => "INSERT INTO `test`
												SET
													datetime='".date("Y-m-d H:i:s")."',
													first_name='".$_POST2['first_name']."',
													last_name='".$_POST2['last_name']."'
											",
							),
							'email' => array(
								'send_email' => true, // true / false
								'from_email' => "noreply@".str_replace("www.", "", $_SERVER['SERVER_NAME']), // Email address it will show as sent from
								'from_name' => "noreply@".str_replace("www.", "", $_SERVER['SERVER_NAME']), // This will be shown as the name from the sender
								'to_email' => array($email), // Array of email addresses the form will be sent to (null will skip this section of the email submission)
								'to_name' => array($email), // Array of email addresses the form will be sent to (null will skip this section of the email submission)
								'subject' => "Password Reset for " . $account['first_name'], // Subject of the email
								'replace_fields' => array(// Replaces preset fields with new values in an emailer//use body needs to be disabled//and values have to be defined before
									'enabled' => false,
									'array_mapping' => $array_mapping,
								),
								'use_custom_body' => false, // enable if using same body as main confirmation email
								'body' => '',
							),
							'email_confirmation' => array(
								'send_email' => false, // true / false
								'from_email' => "noreply@".str_replace("www.", "", $_SERVER['SERVER_NAME']), // Email address it will show as sent from
								'from_name' => "noreply@".str_replace("www.", "", $_SERVER['SERVER_NAME']), // This will be shown as the name from the sender
								'to_email' => array($_POST2['email']), // Array of email addresses the form will be sent to
								'to_name' => array($_POST2['email']), // Array of email addresses the form will be sent to
								'subject' => "Confirmation for ".$_POST2['full_name'], // Subject of the email
								'replace_fields' => array(// Replaces preset fields with new values in an emailer//use body needs to be disabled//and values have to be defined before
									'enabled' => false,
									'array_mapping' => $array_mapping,
								),
								'use_custom_body' => false, // enable if using same body as main confirmation email
								'body' => file_get_contents(dirname(__FILE__) . '/../../portal/email/email.php'),
							),
						);

		//submit form api
			return parent::submitForm($form_settings, $vars, $_FILES2);

		}

	}

}

?>