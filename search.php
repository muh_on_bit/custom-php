<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends CI_Controller
{

	private $data = array();
	
	private $type = 'business'; // $_GET['t'] search type
	
	private $category;
	
	private $sub_category;
	
	private $cat_url;
	
	private $subcat_url;
	
	private $query; // $_GET['q'] search term
	
	private $scope = ''; // $_GET['s'] where to search
	
	private $json = false; // $_GET['j'] true or false
	
	private $bulb = false;
	
	private $results_per_page = 10;
	
	private $location;
	
	private $review_length = 60;
	
	private $mysql = false; // search in mysql instead of sphinx
	
	private $match_geo_locality = false;
	
	private $search_location;
	
	
	function __construct()
	{
		parent::__construct();

		$this->load->model('mod_search');
		
		$this->load->model('mod_review');
		
		$this->load->model('mod_business');
		
		$this->load->model('mod_category');
		
		$get = $this->input->get();
		
		$this->data = $get;
		
		if(isset($this->data['lc']))
		{
			$locality = explode('|', str_replace('_','|_',$this->data['lc']));
			
			$this->data['locale'] = array_shift($locality);
			
			$this->data['scope'] = array_shift($locality);
		}
		
		if(!empty($get))
		{
			if(isset($get['t']))
			{
				$this->type = $get['t'];
			}
			
			if(isset($get['q']))
			{
				$this->query = str_replace('-', ' ', $get['q']);
			}
			
			if(isset($get['s']))
			{
				$this->scope = $get['s'];
			}
			
			if(isset($get['j']))
			{
				$this->json = $get['j'];
			}
		}
		else
		{
			$params = explode('/', urldecode(uri_string()));
			
			array_shift($params);
			
			$this->type = array_shift($params);
			
			if($this->type == 'category')
			{
				$this->category = array_shift($params);
			}
			
		//	$this->query = str_replace('-', ' ', array_shift($params));
			
			$this->data['city'] = substr(str_replace('-', ' ', array_shift($params)),0, -3);
		}
		
		$this->set_location($this->data);
		
		if(isset($this->data['loc']))
		{
			$loc = array_map('trim', explode(',', $this->data['loc']));
			
			$sz = count($loc);
			
			$place = $this->mod_location->find_location($loc);
			
			if(!empty($place))
			{
				if(!is_null($place->hood))
				{
			//		$this->location['neighborhood'] = $place->hood_id;
				}
				
				if(!is_null($place->city))
				{
					$this->location['city'] = $place->city;
				}
				
				if(!is_null($place->province_code))
				{
					$this->location['province_code'] = $place->province_code;
				}
				
				if(!is_null($place->province))
				{
					$this->location['province'] = $place->province;
				}
			}
			
		}
		
	}
	
	function get_bulb()
	{
		return $this->bulb;
	}
	
	function set_bulb($bool)
	{
		$this->bulb = $bool;
		
		$this->index();
	}
	
	
	private function set_location($data)
	{
		if(isset($data['n'])) $this->location['neighborhood'] = $data['n'];
		
		if(isset($data['c'])) $this->location['city'] = $data['c'];
		
		if(isset($data['pc'])) $this->location['province_code'] = $data['pc'];
		
		if(isset($data['p'])) $this->location['province'] = $data['p'];
		
		if(isset($data['cc'])) $this->location['country_code'] = $data['cc'];
		
		if(isset($data['g'])) $this->location['country'] = $data['g'];
		
		if(isset($this->location['city']))
		{
			$location = $this->mod_location->isNeighborhood($this->location['city'], $this->location['province_code']);
			
			if($location)
			{
				$this->match_geo_locality = $location;
				
				$this->data['n'] = $this->location['neighborhood'] = $location->id;
			}
		}
	}
	
	private function is_country($country)
	{
		$list = array ('AD' => 'Andorra','AE' => 'United Arab Emirates','AF' => 'Afghanistan','AG' => 'Antigua and Barbuda','AI' => 'Anguilla','AL' => 'Albania','AM' => 'Armenia','AN' => 'Netherlands Antilles','AO' => 'Angola','AQ' => 'Antarctica','AR' => 'Argentina','AS' => 'American Samoa','AT' => 'Austria','AU' => 'Australia','AW' => 'Aruba','AX' => 'Åland Islands','AZ' => 'Azerbaijan','BA' => 'Bosnia and Herzegovina','BB' => 'Barbados','BD' => 'Bangladesh','BE' => 'Belgium','BF' => 'Burkina Faso','BG' => 'Bulgaria','BH' => 'Bahrain','BI' => 'Burundi','BJ' => 'Benin','BL' => 'Saint Barthélemy','BM' => 'Bermuda','BN' => 'Brunei','BO' => 'Bolivia','BQ' => 'British Antarctic Territory','BR' => 'Brazil','BS' => 'Bahamas','BT' => 'Bhutan','BV' => 'Bouvet Island','BW' => 'Botswana','BY' => 'Belarus','BZ' => 'Belize','CA' => 'Canada','CC' => 'Cocos [Keeling] Islands','CD' => 'Congo - Kinshasa','CF' => 'Central African Republic','CG' => 'Congo - Brazzaville','CH' => 'Switzerland','CI' => 'Côte d’Ivoire','CK' => 'Cook Islands','CL' => 'Chile','CM' => 'Cameroon','CN' => 'China','CO' => 'Colombia','CR' => 'Costa Rica','CS' => 'Serbia and Montenegro','CT' => 'Canton and Enderbury Islands','CU' => 'Cuba','CV' => 'Cape Verde','CX' => 'Christmas Island','CY' => 'Cyprus','CZ' => 'Czech Republic','DD' => 'East Germany','DE' => 'Germany','DJ' => 'Djibouti','DK' => 'Denmark','DM' => 'Dominica','DO' => 'Dominican Republic','DZ' => 'Algeria','EC' => 'Ecuador','EE' => 'Estonia','EG' => 'Egypt','EH' => 'Western Sahara','ER' => 'Eritrea','ES' => 'Spain','ET' => 'Ethiopia','FI' => 'Finland','FJ' => 'Fiji','FK' => 'Falkland Islands','FM' => 'Micronesia','FO' => 'Faroe Islands','FQ' => 'French Southern and Antarctic Territories','FR' => 'France','FX' => 'Metropolitan France','GA' => 'Gabon','GB' => 'United Kingdom','GD' => 'Grenada','GE' => 'Georgia','GF' => 'French Guiana','GG' => 'Guernsey','GH' => 'Ghana','GI' => 'Gibraltar','GL' => 'Greenland','GM' => 'Gambia','GN' => 'Guinea','GP' => 'Guadeloupe','GQ' => 'Equatorial Guinea','GR' => 'Greece','GS' => 'South Georgia and the South Sandwich Islands','GT' => 'Guatemala','GU' => 'Guam','GW' => 'Guinea-Bissau','GY' => 'Guyana','HK' => 'Hong Kong SAR China','HM' => 'Heard Island and McDonald Islands','HN' => 'Honduras','HR' => 'Croatia','HT' => 'Haiti','HU' => 'Hungary','ID' => 'Indonesia','IE' => 'Ireland','IL' => 'Israel','IM' => 'Isle of Man','IN' => 'India','IO' => 'British Indian Ocean Territory','IQ' => 'Iraq','IR' => 'Iran','IS' => 'Iceland','IT' => 'Italy','JE' => 'Jersey','JM' => 'Jamaica','JO' => 'Jordan','JP' => 'Japan','JT' => 'Johnston Island','KE' => 'Kenya','KG' => 'Kyrgyzstan','KH' => 'Cambodia','KI' => 'Kiribati','KM' => 'Comoros','KN' => 'Saint Kitts and Nevis','KP' => 'North Korea','KR' => 'South Korea','KW' => 'Kuwait','KY' => 'Cayman Islands','KZ' => 'Kazakhstan','LA' => 'Laos','LB' => 'Lebanon','LC' => 'Saint Lucia','LI' => 'Liechtenstein','LK' => 'Sri Lanka','LR' => 'Liberia','LS' => 'Lesotho','LT' => 'Lithuania','LU' => 'Luxembourg','LV' => 'Latvia','LY' => 'Libya','MA' => 'Morocco','MC' => 'Monaco','MD' => 'Moldova','ME' => 'Montenegro','MF' => 'Saint Martin','MG' => 'Madagascar','MH' => 'Marshall Islands','MI' => 'Midway Islands','MK' => 'Macedonia','ML' => 'Mali','MM' => 'Myanmar [Burma]','MN' => 'Mongolia','MO' => 'Macau SAR China','MP' => 'Northern Mariana Islands','MQ' => 'Martinique','MR' => 'Mauritania','MS' => 'Montserrat','MT' => 'Malta','MU' => 'Mauritius','MV' => 'Maldives','MW' => 'Malawi','MX' => 'Mexico','MY' => 'Malaysia','MZ' => 'Mozambique','NA' => 'Namibia','NC' => 'New Caledonia','NE' => 'Niger','NF' => 'Norfolk Island','NG' => 'Nigeria','NI' => 'Nicaragua','NL' => 'Netherlands','NO' => 'Norway','NP' => 'Nepal','NQ' => 'Dronning Maud Land','NR' => 'Nauru','NT' => 'Neutral Zone','NU' => 'Niue','NZ' => 'New Zealand','OM' => 'Oman','PA' => 'Panama','PC' => 'Pacific Islands Trust Territory','PE' => 'Peru','PF' => 'French Polynesia','PG' => 'Papua New Guinea','PH' => 'Philippines','PK' => 'Pakistan','PL' => 'Poland','PM' => 'Saint Pierre and Miquelon','PN' => 'Pitcairn Islands','PR' => 'Puerto Rico','PS' => 'Palestinian Territories','PT' => 'Portugal','PU' => 'U.S. Miscellaneous Pacific Islands','PW' => 'Palau','PY' => 'Paraguay','PZ' => 'Panama Canal Zone','QA' => 'Qatar','RE' => 'Réunion','RO' => 'Romania','RS' => 'Serbia','RU' => 'Russia','RW' => 'Rwanda','SA' => 'Saudi Arabia','SB' => 'Solomon Islands','SC' => 'Seychelles','SD' => 'Sudan','SE' => 'Sweden','SG' => 'Singapore','SH' => 'Saint Helena','SI' => 'Slovenia','SJ' => 'Svalbard and Jan Mayen','SK' => 'Slovakia','SL' => 'Sierra Leone','SM' => 'San Marino','SN' => 'Senegal','SO' => 'Somalia','SR' => 'Suriname','ST' => 'São Tomé and Príncipe','SU' => 'Union of Soviet Socialist Republics','SV' => 'El Salvador','SY' => 'Syria','SZ' => 'Swaziland','TC' => 'Turks and Caicos Islands','TD' => 'Chad','TF' => 'French Southern Territories','TG' => 'Togo','TH' => 'Thailand','TJ' => 'Tajikistan','TK' => 'Tokelau','TL' => 'Timor-Leste','TM' => 'Turkmenistan','TN' => 'Tunisia','TO' => 'Tonga','TR' => 'Turkey','TT' => 'Trinidad and Tobago','TV' => 'Tuvalu','TW' => 'Taiwan','TZ' => 'Tanzania','UA' => 'Ukraine','UG' => 'Uganda','UM' => 'U.S. Minor Outlying Islands','US' => 'United States','UY' => 'Uruguay','UZ' => 'Uzbekistan','VA' => 'Vatican City','VC' => 'Saint Vincent and the Grenadines','VD' => 'North Vietnam','VE' => 'Venezuela','VG' => 'British Virgin Islands','VI' => 'U.S. Virgin Islands','VN' => 'Vietnam','VU' => 'Vanuatu','WF' => 'Wallis and Futuna','WK' => 'Wake Island','WS' => 'Samoa','YD' => 'People\'s Democratic Republic of Yemen','YE' => 'Yemen','YT' => 'Mayotte','ZA' => 'South Africa','ZM' => 'Zambia','ZW' => 'Zimbabwe','ZZ' => 'Unknown or Invalid Region');
		
		return array_key_exists(trim(strtoupper($country)), $list);
	}
	
	function index($location=null, $cat_url=null, $subcat_url=null)
	{
		
		if(!is_null($location))
		{
			$this->cat_url = $cat_url;
			
			$this->subcat_url = $subcat_url;
			
			$loc = explode('-', urldecode($location));
			
			$i = count($loc);
			
			$region = array_pop($loc);
			
			$region_type = $this->is_country($region) ? 'cc' : 'pc';
			
			if($i == 1)
			{
				if(method_exists($this, $region))
				{
					call_user_func_array(array($this, $region), array());
					
					return;
				}
				elseif(false != ($found = $this->mod_search->find_location($region)))
				{
					
					$this->search_location = $found;
					
					foreach($found[0] as $k => $v)
					{
						$this->location[$k] = $v;
					}
					
					if(!is_null($this->location['city']))
					{
						$region_type = 'pc';
						
						$region = $this->location['province_code'];
					}
				}
				else
				{
                                    set_status_header(404,"Location Not Found");
				}
			
			}
			
			$place = array($region_type=>strtoupper($region));
			
			if($i == 2)
			{
				$place_type = $region_type == 'pc' ? 'c' : 'p';
				
				$place[$place_type] = ucfirst(array_pop($loc));
			}
			elseif($i >= 3)
			{
				if($region_type == 'pc')
				{
					$place['c'] = ucwords(implode($loc, ' '));
				}
				else
				{
					$region = strtoupper(array_pop($loc));
					
					if(strlen($region) == 2) $place['pc'] = $region;
					
					$place['c'] = ucwords(implode($loc, ' '));
				}
			}
			
			$this->set_location($place);
			
			$this->type = 'business';
		}
		
		$method = ($this->type == 'business' || $this->type == 'category') ? 'business' : $this->type;
		
		if(!method_exists($this, $method)) $method = 'business';

		$results = call_user_func_array(array($this, $method), array());
		
                if($this->json)
		{
			if(!empty($results) && is_array($results[0]))
			{
				foreach($results[0] as &$result)
				{
					if(is_array($result))
					{
						$result = array_map(
						
							function($x)
							{
								if(is_string($x))
								{
									return mb_convert_encoding($x,'UTF-8','UTF-8');
								}
								
								return $x;
							}, 
						$result);
					}
				}
			}
						
		
			foreach($results as &$result)
			{
				
				if(isset($result['city']) && $result['city']=='Totonto'){	
				unset($results[0]);
				}
				
				if (is_array($result)) {
                    $result = array_map(function($string) {
                        if (!is_array($string)) {
							
                            return mb_convert_encoding($string, 'UTF-8', 'UTF-8');
                        } else {
                            return $string;
                        }
                    }, $result);
                }
            }
			
			
			echo str_replace('?','',json_encode($results));
			
			exit;
		}
		
		$data = array();
		
		$data['page'] = 'search';
		
		$data['data']['type'] = $method;
		
		$data['data']['query'] = $this->query;
		
		if(isset($results[0]) && !empty($results[0]))
		{
			foreach($results[0] as &$result)
			{
				if(is_array($result))
				{
					$result = array_map(
					
						function($x)
						{
							if(is_string($x))
							{
								return mb_convert_encoding($x,'UTF-8','UTF-8');
							}
							
							return $x;
						}, 
					$result);
				}
			}
		}
		else
		{
			$data['zero'] = true;
		}
		
		$data['data']['results'] = $results;
		
		$data['isCatSearch'] = true;
		/*
		if(isset($this->data['te']))
		{
			$segments = array_map('ucfirst', explode('-', $this->data['te']));
			
			$cat_id = array_pop($segments);
			
			$catinfo = $this->mod_category->get_category($cat_id);
			
			$data['data']['search_category'][0] = array($catinfo->name, $catinfo->id);
			
			$data['data']['cats'] = $this->mod_category->fetch_children($cat_id);
		}
		
		if(isset($this->data['ste']))
		{
			$segments = array_map('ucfirst', explode('-', $this->data['ste']));
			
			$subcat_id = array_pop($segments);
			
			$catinfo = $this->mod_category->get_category($subcat_id);
			
			$data['data']['search_category'][1] = array($catinfo->name, $catinfo->id);
			
			$data['data']['features'] = $this->mod_category->get_features($subcat_id);
		}
		*/
        if ($this->cat_url != null) {
            $cat = $this->mod_category->get_cat_by_url($this->cat_url);
            if (!empty($cat) && $this->subcat_url != null) {
                $cat = $this->mod_category->get_cat_by_url($this->subcat_url , $cat->id);
            }
        }
        if (isset($cat) && !empty($cat)) {
            $this->category = $cat;
        }
        
		if(is_object($this->category))
		{
			$cat_id = ($this->category->{'parent'} == 0) ?  $this->category->id : $this->category->{'parent'};
			
			if($this->category->{'parent'} == 0)
			{
				$data['data']['main_cat'] = $this->category;
                $data['data']['cats'] = $this->mod_category->fetch_children($cat_id);
			}
			else
			{
				$data['data']['main_cat'] = $this->mod_category->get_category($this->category->{'parent'});
                $data['data']['cats'] = $this->mod_category->fetch_children($this->category->{'parent'});
			}
			
			$data['data']['cats'] = $this->mod_category->fetch_children($cat_id);
		}
		
		if(isset($cat_id))
		{
			$data['data']['features'] = $this->mod_category->get_features($this->category->id);
			
			if(empty($data['data']['features']))
			{
				$data['data']['features'] = $this->mod_category->get_features($cat_id);
			}
		}
		else
		{
			$data['isCatSearch'] = false;
			
			$data['data']['features'] = $this->mod_category->get_features();
			
			$data['data']['cats'] = $this->mod_category->fetch_all();
		}
		
		if(isset($this->location['city']))
		{
			$places = $this->mod_search->get_places($this->location['city'], $this->location['province_code'], 'city', $this->match_geo_locality);
			
			if(!empty($places))
			{
				foreach($places as $k => $v) $data['data'][$k] = $v;
			}
		}
		elseif(isset($this->location['province']))
		{
			$places = $this->mod_search->get_places($this->location['province'], $this->location['country_code'], 'province');
			
			if(!empty($places))
			{
				foreach($places as $k => $v) $data['data'][$k] = $v;
			}
		}
		
		$data['data']['results_per_page'] = $this->results_per_page;
		
		if(isset($data['data']['search_category'][0]))
		{
			$data['title'] = $data['data']['search_category'][0][0];
		}
		else
		{
			$data['title'] = $this->type;
		}
		
		$data['title'] .= ' search ';
		
		if(!empty($this->query)) $data['title'] .= 'for '.$this->query;
		
		$data['title'] .= ' - verascore';
		
		$data['title'] = ucwords($data['title']);
		
		$data['__script'] = 'maps';
        
        if (!empty($this->location)){
            $data['data']['queryLocation'] = $this->mod_search->get_location($this->location['city'], $this->location['province_code']);
        }
		//print_r($data);exit;
		$this->load->view('template', $data);
		
	}

	function suggestion($query = null)
	{
		$get = $this->input->get(NULL, TRUE);
		
		$query = $get['q'];
		
		$city = (isset($get['city']) && !empty($get['city'])) ? $get['city'] : null;
		
		$province = (isset($get['province']) && !empty($get['province'])) ? $get['province'] : null;
		
		if(is_null($city) && is_null($province) && isset($get['l']))
		{
			$location = explode(',', $get['l']);
			
			if(count($location) == 2)
			{
				$city = trim($location[0]);
				
				$province = trim($location[1]);
			}
		}
		
		$suggestions = $this->mod_search->suggest($query, $city, $province);
		
		$s = array(' ', '?');
		
		$r = array('-', '');
		
		foreach($suggestions as &$suggest)
		{
			if(!empty($suggest['business_id']))
			{
				$suggest['category'] = trim(substr($suggest['cat_assigned'], 0, strpos($suggest['cat_assigned'], ':')));
				
				$suggest['url'] = strtolower(str_replace($s,$r,$suggest['term']));
			}
		}
		
		return $suggestions;
	}
	
	// only for displaying member search page
	function member($query = null)
	{		
		$data = array();
		
		$data['page'] = 'search/member';
		
		$this->load->view('template', $data);
	}
	
	//user search
	function user()
	{
		$results = $this->mod_search->find_user($this->query);
		
		return $results;
	}
	
	// business and category search
	function business()
	{
		/*
		$cat = isset($this->data['te']) ? array(array_pop(explode('-',$this->data['te']))) : null;
		
		$subcats = isset($this->data['ste']) ? explode(',',$this->data['ste']) : null;
		
		$subcat_list = array();
		
		if(!empty($subcats))
		{
			foreach($subcats as $cat)
			{
				!empty($cat) && array_push($subcat_list, array_pop(explode('-', $cat)));
			}
		}
		
		$cat = !empty($subcat_list) ? $subcat_list : $cat;
		*/
		$offset = isset($this->data['page']) ? ($this->data['page']-1) * $this->results_per_page : 0;
		
		$sort = array('distance'=>'asc');
		
		$filters = array();
		
		$distance = null;
		
		$sort_keys = array('rt'=>'rating');
		
		foreach($sort_keys as $key => $column)
		{
			if(isset($this->data[$key]) && !empty($this->data[$key]))
			{
				$sort[$column] = $this->data[$key];
			}
		}
		
		$filter_keys = array('ft'=>'features','dl'=>'cost'); // 'fr'=>'friends','op'=>'time'
		
		foreach($filter_keys as $key => $column)
		{
			if(isset($this->data[$key]) && !empty($this->data[$key]))
			{
				$filters[$column] = $this->data[$key];
			}
		}
                                
		
		$perimeter_keys = array('x'=>'distance');
		
		foreach($perimeter_keys as $key => $column)
		{
			if(isset($this->data[$key]) && !empty($this->data[$key]))
			{
				$distance = $this->data[$key];
			}
		}
		
		$cat = null;
		  
		//if($this->cat_url)
		if($this->input->get('cat') != '')
		{
                    $newcat = $this->input->get('cat');
			$cat = $this->mod_category->get_cat_by_url($newcat);
			
			if($cat && $this->subcat_url)
			{
				$cat = $this->mod_category->get_cat_by_url($this->subcat_url, $cat->id);
				
				$cat->has_parent = true;
			}
			
			$this->category = $cat;
		}
		
		if(!$this->mysql)
		{
			/*SPHINX*/
			$google = isset($this->data['google']) && $this->data['google'] ? true : false;
			
			$this->mod_search->set_google_status($google);
			
			$results = $this->mod_search->search_business($this->query, $cat, $this->location, $distance, $sort, $filters, $offset, $this->results_per_page);

			if($google) return array();
			
		//	$coords = $this->mod_search->get_geo_coords();
			
		//	if($coords) $this->load->library('geolocation');
			
			foreach($results[0] as &$business)
			{
				if(isset($business['id']))
				{
					$business_id = $business['id'];
					
					$review = $this->mod_review->get_reviews($business_id, 0, 1);
					
					$photo = $this->mod_business->get_business_thumbs($business_id, 1);
					
					if($business['latitude'] != 0 || $business['longitude'] != 0)
					{
						$business['distance'] = round($business['distance']/1000, 1);
					}
					else
					{
						$business['distance'] = 0;
					}
					
					if($review)
					{
						$review['summary'] = substr($review[0]['summary'], 0, $this->review_length);
						
						$business['review'] = $review;
					}
					
					if($photo)
					{
						$business['photo'] = $photo;
					}
				}
			}
			
			return $results;
		}
		elseif($this->mysql)
		{
			/*MYSQL*/
			
			$results = $this->mod_search->search_business_mysql($this->query, $cat, $this->location, $distance, $sort[0], $sort[1], $offset, $this->results_per_page, isset($this->data['j']));
		
			foreach($results[0] as $result => &$business)
			{
				if(isset($business['id']))
				{
					$business_id = $business['id'];
					
					$review = $this->mod_review->get_reviews($business_id, 0, 1);
					
					$photo = $this->mod_business->get_business_thumbs($business_id, 1);
					
					if(!empty($review))
					{
						$review[0]['summary'] = substr($review[0]['summary'], 0, $this->review_length);
						
						$business['review'] = $review;
					}
					
					if($photo)
					{
						$business['photo'] = $photo;
					}
				}
			}
				
			return $results;
		}
	}
	
	
	// city search
	function city()
	{
		$province = isset($this->data['pid']) && $this->data['pid'] > 0 ? $this->data['pid'] : null;
		
		$results = $this->mod_search->find_city($this->query, $province);
		
		return $results;
	}
    
    // category search
    function qcategory()
    {
        $results = $this->mod_search->find_categories($this->query);
        
        return $results;
    }
	
	// location search
	function location()
	{
		$results = $this->mod_search->find_location($this->query);
		
		return $results;
	}
	
	//community search
	function community($keyword = null)
	{
		if(!is_null($keyword))
		{
			$this->query = $keyword;
			
			$this->type = 'community';
			
			$this->set_bulb(true);
		}
		else
		{
			if($this->get_bulb())
			{
				$results = $this->mod_search->find_community_keyword($this->query);
			}
			else
			{
				$results = $this->mod_search->find_in_community($this->query);
			}
			
			return $results;
		}
	}
	
	function find()
	{
		$data = $this->input->get();
		
		if(strlen($data['q']) >= 3)
		{
			$results = $this->xql->db()->select('*')->from('vera_search')->match('name, city', $data['q'])->execute();
			
			echo json_encode($results);
		}
	}

	/*
	function index()
	{
		$type = $_GET['type'];
			$city = $_GET['city'];
			$query = $_GET['q'];
			
		$this->lists($city,$type,$query,$ordertype='asc',$orderby='name');
	}
	*/
	

	function lists($city_name='',$kwd='',$query='',$ordertype='desc',$orderby='rating',$is_sponsored=0,$page_no = 1)
	{ 
		$this->load->library('pagination');
		$this->load->model('Bs_model','bs');
		$get = $this->input->get(NULL, TRUE);
		$type = $kwd;
		$city = $city_name;
		$query = $query;
		$businesses_per_page = 10; //$this->system->settings['records_per_page']
		if($page_no=="")
      $page_no = 1;
		//$is_sponsored = 0;
		$data = array();
		$data['sub_sub_sub_cat_ids']='';
		$data['sub_subcategories']=array();
		$data['neighbours']=array();
		$data['city_id']=0;
		$data['url_sub_cat_name'] = '';
		$data['subcat_id'] = 0;
		$data['friends_only'] = 0;
		$data['url_city_name'] = urldecode($city); 
    $this->db->select('geo_city.*');
		$this->db->from('geo_city');
		$this->db->where('geo_city.name', $city);
   // $this->db->where('geo_province.country_code', 'CA');
    $this->db->join('geo_province','geo_province.id = geo_city.province_id','left');
		$profile = $this->db->get();
    $cityarr = $profile->result_array();
   // echo $this->db->last_query();
   // var_dump($cityarr);
    $city_id  = intval($cityarr[0]['city_id']);
    $limit=$businesses_per_page;
    $offset = (($page_no-1)* ($limit));
    //var_dump($city_id);
    //$this->bs->xql_verasearch_update('59028');
    
    /*$sql = "select registrar_id from bs_register ";
    $total_results = $this->db->query($sql)->result_array();
    
    //$total_results = $this->db()->select("registrar_id")->from('bs_register')->orderBy("id","asc")->limit(0, 50000)->execute();
    
    set_time_limit(0);
    //$total_results = $this->xql->db()->select()->from('vera_search')->orderBy("id","asc")->limit(0, 50000)->execute();
    foreach($total_results as $result)
    {
      //$this->xql->db()->delete()->from('vera_search')->where('id', '=', intval($result["id"]))->execute();
      $this->bs->xql_verasearch_update($result["registrar_id"]);
    }*/    
    
    //$results = $this->xql->db()->select("id")->from('vera_search')->orderBy("id","asc")->limit(0, 100)->execute();
    
    
    
    
    //$this->xql->db()->delete()->from('vera_search')->execute();
    //echo $city_id;
    if($query != "all"){
    //echo $city_id;
    echo "here";
    $total_results = $this->xql->db()->select("id")->from('vera_search')->match('*',$query)->where("city_id",$city_id)->execute();
    
    //echo $this->xql->db()->last_query();
    //var_dump($total_results);
    $results = $this->xql->db()->select()->from('vera_search')
				->match('*',$query)
				->where('city_id',$city_id)
				
				->orderBy($orderby,$ordertype)
				->limit($offset, $limit)
				->execute();
	
	//echo $this->xql->db()->last_query();
    }else{
//      echo $city_id;
     //echo "here1".$offset." - ".$limit;
     $total_results = $this->xql->db()->select("id")->from('vera_search')->where("city_id",$city_id)->limit(0, 1000)->execute();
     //var_dump($this->xql->db());
     //print_r($total_results);
    
    $results = $this->xql->db()->select()->from('vera_search')->where("city_id",$city_id)->orderBy($orderby,$ordertype)->limit($offset, $limit)->execute();
    }
    //echo $this->xql->db()->last_query();
    //print_r($results);
    //print_r($results); exit;
    foreach($results as &$biz)
		{
			$biz['meta_lng']= $biz['longitude'];
			$biz['meta_lat'] = $biz['latitude'];
			$biz['meta_postalcode'] = $biz['postal_code'];
			$biz['company'] = $biz['name'];
			$biz['numreviews'] = $biz['numreviews'];
			$biz['agvrating'] = $biz['agvrating'];
			$biz['is_sponsored'] = 0;
			$biz['address'] = $biz['street_line_1'].' '.$biz['street_line_2'].' '.$biz['city'].' '.$biz['province'].' '.$biz['postal_code'];
			$biz['phone'] = '';
			$biz['business_photo'] = null;
			
		}
		//var_dump($results);
	 //	$data['city_id'] = $this->city->get_city_id($city);
		$data['businesses_per_page'] = $businesses_per_page;
		$data['is_sponsored'] = $is_sponsored;  			
		$data['city']['full_name_nd'] = $city;
		$data['business_current_page'] = $page_no;
		$data['businesses_ordertype'] = $ordertype;
		$data['businesses_orderby'] = $orderby;
    $data['type']=$type;
		$data['query']=$query;
		$data['url_kwd']=$query;
		$data['url_city']=$city_name;
		
	//	if($orderby=='name') $data['businesses_ordertype'] = 'asc';
		$data['total_businesses'] = count($total_results);
		$data['businesses'] = $results;
		$filter_url = $city_name."/".$kwd."/".$query."/".$ordertype."/".$orderby."/".$is_sponsored."/"; 
    $main_url = "search/lists/".$filter_url;
    $page["cur_page"]= $page_no;
    if($page["cur_page"] == 0)
    $page["cur_page"] = 1;
    $data["cur_page"] = $page["cur_page"];
    $page['first_link'] = '';
    $page['first_tag_open'] = '<li>';
    $page['first_tag_close'] = '</li>';

    $page['last_link'] = '';
    $page['last_tag_open'] = '<li>';
    $page['last_tag_close'] = '</li>';
    $page['next_link'] = '<span class="active">Next</span>';
    $page['next_tag_open'] = '<li>';
    $page['next_tag_close'] = '</li>';
    $page['prev_link'] = '<span class="active">Prev</span>';
    $page['prev_tag_open'] = '<li>';
    $page['prev_tag_close'] = '</li>';
    
    $page['cur_tag_open'] = '<li class="active">';
    $page['cur_tag_close'] = '</li>';
    
    $page['num_tag_open'] = '<li>';
    $page['num_tag_close'] = '</li>';
    
    $page["base_url"]= base_url().$main_url;
     
    $page["total_rows"]=$data['total_businesses'];
    $page["per_page"]=$businesses_per_page;
    $page["num_links"]=2;
     
    $this->pagination->initialize($page);
    //print_r($data); exit;
		$this->load->view('main/header',$data);
    if($_REQUEST['ajax']=="true" && $_REQUEST['ajax']!=""){
		  $this->load->view('pages/business_listing',$data);
    }else{
      $this->load->view('main/business_listing',$data);
    }
		$this->load->view('main/footer',$data);
	}
}?>
